﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Script.Services;
using ZstdSharp.Unsafe;
using static PMRes;

namespace sopvn_wcf
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPMService" in both code and config file together.
    [ServiceContract]
    public interface IPMService
    {
        #region Hr
        [OperationContract(Name = "NewHr")]
        Result<HR_DTO> New(HR item);
        [OperationContract(Name = "NewHrs")]
        Result<List<HR_DTO>> NewMany(List<HR> items);
        [OperationContract(Name = "UpdateHrAvatar")]
        Result<HR_DTO> UpdateAvatar(HR item);
        [OperationContract(Name = "UpdateHrPassword")]
        Result<HR_DTO> UpdatePassword(HR item);
        [OperationContract(Name = "UpdateHr")]
        Result<HR_DTO> Update(HR item);
        [OperationContract(Name = "UpdateHrs")]
        Result<List<HR_DTO>> UpdateMany(List<HR> items);
        [OperationContract(Name = "ChangeHrStatus")]
        Result<HR_DTO> ChangeStatus(HR item);
        [OperationContract(Name = "DeleteHrs")]
        Result<List<HR_DTO>> DeleteMany(List<HR> items);
        [OperationContract(Name = "GetHr")]
        Result<HR_DTO> Get(HR item);
        [OperationContract(Name = "GetHrs")]
        Result<List<HR_DTO>> Gets(HR item);
        [OperationContract(Name = "IsExistsHr")]
        Result<HR_DTO> IsExists(HR item);
        #endregion

        #region Permision
        [OperationContract(Name = "NewPermision")]
        Result<PER_DTO> New(PER item);
        [OperationContract(Name = "NewPermisions")]
        Result<List<PER_DTO>> NewMany(List<PER> items);
        [OperationContract(Name = "UpdatePermision")]
        Result<PER_DTO> Update(PER item);
        [OperationContract(Name = "UpdatePermisions")]
        Result<List<PER_DTO>> UpdateMany(List<PER> items);
        [OperationContract(Name = "ChangePermisionStatus")]
        Result<PER_DTO> ChangeStatus(PER item);
        [OperationContract(Name = "DeletePermisions")]
        Result<List<PER_DTO>> DeleteMany(List<PER> items);
        [OperationContract(Name = "GetPermision")]
        Result<PER_DTO> Get(PER item);
        [OperationContract(Name = "GetPermisions")]
        Result<List<PER_DTO>> Gets(PER item);
        [OperationContract(Name = "IsExistsPermision")]
        Result<PER_DTO> IsExists(PER item);
        #endregion

        #region Position
        [OperationContract(Name = "NewPosition")]
        Result<POS_DTO> New(POS item);
        [OperationContract(Name = "NewPositions")]
        Result<List<POS_DTO>> NewMany(List<POS> newItem);
        [OperationContract(Name = "UpdatePosition")]
        Result<POS_DTO> Update(POS item);
        [OperationContract(Name = "UpdatePositions")]
        Result<List<POS_DTO>> UpdateMany(List<POS> newItem);
        [OperationContract(Name = "ChangePositionStatus")]
        Result<POS_DTO> ChangeStatus(POS item);
        [OperationContract(Name = "DeletePositions")]
        Result<List<POS_DTO>> DeleteMany(List<POS> items);
        [OperationContract(Name = "GetPosition")]
        Result<POS_DTO> Get(POS item);
        [OperationContract(Name = "GetPositions")]
        Result<List<POS_DTO>> Gets(POS item);
        [OperationContract(Name = "IsExistsPosition")]
        Result<POS_DTO> IsExists(POS item);
        #endregion

        #region Project
        [OperationContract(Name = "NewProject")]
        Result<PRO_DTO> New(PRO item);
        [OperationContract(Name = "NewProjects")]
        Result<List<PRO_DTO>> NewMany(List<PRO> items);
        [OperationContract(Name = "UpdateProject")]
        Result<PRO_DTO> Update(PRO item);
        [OperationContract(Name = "UpdateProjects")]
        Result<List<PRO_DTO>> UpdateMany(List<PRO> items);
        [OperationContract(Name = "ChangeProjectStatus")]
        Result<PRO_DTO> ChangeStatus(PRO item);
        [OperationContract(Name = "DeleteProjects")]
        Result<List<PRO_DTO>> DeleteMany(List<PRO> items);
        [OperationContract(Name = "GetProject")]
        Result<PRO_DTO> Get(PRO item);
        [OperationContract(Name = "GetProjects")]
        Result<List<PRO_DTO>> Gets(PRO item);
        [OperationContract(Name = "IsExistsProject")]
        Result<PRO_DTO> IsExists(PRO item);
        #endregion

        #region Project Permision
        [OperationContract(Name = "NewProjectPermision")]
        Result<PRO_PER_DTO> New(PRO_PER item);
        [OperationContract(Name = "NewProjectPermisions")]
        Result<List<PRO_PER_DTO>> NewMany(List<PRO_PER> items);
        [OperationContract(Name = "UpdateProjectPermision")]
        Result<PRO_PER_DTO> Update(PRO_PER item);
        [OperationContract(Name = "UpdateProjectPermisions")]
        Result<List<PRO_PER_DTO>> UpdateMany(List<PRO_PER> items);
        [OperationContract(Name = "ChangeProjectPermisionStatus")]
        Result<PRO_PER_DTO> ChangeStatus(PRO_PER item);
        [OperationContract(Name = "DeleteProjectPermision")]
        Result<List<PRO_PER_DTO>> DeleteMany(List<PRO_PER> items);
        [OperationContract(Name = "GetProjectPermision")]
        Result<PRO_PER_DTO> Get(PRO_PER item);
        [OperationContract(Name = "GetProjectPermisions")]
        Result<List<PRO_PER_DTO>> Gets(PRO_PER_DTO item);
        [OperationContract(Name = "IsExistsProjectPermision")]
        Result<PRO_PER_DTO> IsExists(PRO_PER item);
        #endregion

        #region Project hr
        [OperationContract(Name = "NewProjectEmployee")]
        Result<PRO_HR_DTO> New(PRO_HR item);
        [OperationContract(Name = "NewProjectEmployees")]
        Result<List<PRO_HR_DTO>> NewMany(List<PRO_HR> items);
        [OperationContract(Name = "UpdateProjectEmployee")]
        Result<PRO_HR_DTO> Update(PRO_HR item);
        [OperationContract(Name = "UpdateProjectEmployees")]
        Result<List<PRO_HR_DTO>> UpdateMany(List<PRO_HR> items);
        [OperationContract(Name = "ChangeProjectEmployeeStatus")]
        Result<PRO_HR_DTO> ChangeStatus(PRO_HR item);
        [OperationContract(Name = "DeleteProjectEmployees")]
        Result<List<PRO_HR_DTO>> DeleteMany(List<PRO_HR> items);
        [OperationContract(Name = "GetProjectEmployee")]
        Result<PRO_HR_DTO> Get(PRO_HR item);
        [OperationContract(Name = "GetProjectEmployees")]
        Result<List<PRO_HR_DTO>> Gets(PRO_HR_DTO item);
        [OperationContract(Name = "IsExsitsProjectEmployee")]
        Result<PRO_HR_DTO> IsExists(PRO_HR item);
        #endregion

        #region Requirement
        [OperationContract(Name = "NewRequirement")]
        Result<REQ_DTO> New(REQ item, string dbName);
        [OperationContract(Name = "NewRequirements")]
        Result<List<REQ_DTO>> NewMany(List<REQ> items, string dbName);
        [OperationContract(Name = "UpdateRequirement")]
        Result<REQ_DTO> Update(REQ item, string dbName);
        [OperationContract(Name = "UpdateRequirements")]
        Result<List<REQ_DTO>> UpdateMany(List<REQ> items, string dbName);
        [OperationContract(Name = "DeleteRequirements")]
        Result<List<REQ_DTO>> DeleteMany(List<REQ> items, string dbName);
        [OperationContract(Name = "GetRequirement")]
        Result<REQ_DTO> Get(REQ item, string dbName);
        [OperationContract(Name = "GetRequirements")]
        Result<List<REQ_DTO>> Gets(REQ item, string dbName);
        [OperationContract(Name = "IsExistsRequirement")]
        Result<REQ_DTO> IsExists(REQ item, string dbName);
        #endregion

        #region User Story
        [OperationContract(Name = "NewUserStory")]
        Result<US_DTO> New(US item, string dbName);
        [OperationContract(Name = "NewUserStories")]
        Result<List<US_DTO>> NewMany(List<US> items, string dbName);
        [OperationContract(Name = "UpdateUserStory")]
        Result<US_DTO> Update(US item, string dbName);
        [OperationContract(Name = "UpdateUserStories")]
        Result<List<US_DTO>> UpdateMany(List<US> items, string dbName);
        [OperationContract(Name = "DeleteUserStories")]
        Result<List<US_DTO>> DeleteMany(List<US> items, string dbName);
        [OperationContract(Name = "GetUserStory")]
        Result<US_DTO> Get(US item, string dbName);
        [OperationContract(Name = "GetUserStories")]
        Result<List<US_DTO>> Gets(US item, string dbName);
        [OperationContract(Name = "IsExistsUserStory")]
        Result<US_DTO> IsExists(US item, string dbName);
        #endregion

        #region Sprint
        [OperationContract(Name = "NewSprint")]
        Result<SPRINT_DTO> New(SPRINT item, string dbName);
        [OperationContract(Name = "NewSprints")]
        Result<List<SPRINT_DTO>> NewMany(List<SPRINT> item, string dbName);
        [OperationContract(Name = "UpdateSprint")]
        Result<SPRINT_DTO> Update(SPRINT item, string dbName);
        [OperationContract(Name = "UpdateSprints")]
        Result<List<SPRINT_DTO>> UpdateMany(List<SPRINT> items, string dbName);
        [OperationContract(Name = "DeleteSprints")]
        Result<List<SPRINT_DTO>> DeleteMany(List<SPRINT> items, string dbName);
        [OperationContract(Name = "GetSprint")]
        Result<SPRINT_DTO> Get(SPRINT item, string dbName);
        [OperationContract(Name = "GetSprints")]
        Result<List<SPRINT_DTO>> Gets(SPRINT item, string dbName);
        [OperationContract(Name = "IsExistsSprint")]
        Result<SPRINT_DTO> IsExists(SPRINT item, string dbName);
        #endregion

        #region Task
        [OperationContract(Name = "NewTask")]
        Result<TASK_DTO> New(TASK item, string dbName);
        [OperationContract(Name = "NewTasks")]
        Result<List<TASK_DTO>> NewMany(List<TASK> items, string dbName);
        [OperationContract(Name = "UpdateTask")]
        Result<TASK_DTO> Update(TASK data, string dbName);
        [OperationContract(Name = "UpdateTasks")]
        Result<List<TASK_DTO>> UpdateMany(List<TASK> items, string dbName);
        [OperationContract(Name = "DeleteTasks")]
        Result<List<TASK_DTO>> DeleteMany(List<TASK> items, string dbName);
        [OperationContract(Name = "GetTask")]
        Result<TASK_DTO> Get(TASK item, string dbName);
        [OperationContract(Name = "GetTasks")]
        Result<List<TASK_DTO>> Gets(TASK_DTO item, string dbName);
        [OperationContract(Name = "IsExistsTask")]
        Result<TASK_DTO> IsExists(TASK item, string dbName);
        #endregion

        #region Sprint Test
        [OperationContract(Name = "NewSprintTest")]
        Result<ST_DTO> New(ST item, string dbName);
        [OperationContract(Name = "NewSprintTests")]
        Result<List<ST_DTO>> NewMany(List<ST> items, string dbName);
        [OperationContract(Name = "UpdateSprintTest")]
        Result<ST_DTO> Update(ST data, string dbName);
        [OperationContract(Name = "UpdateSprintTests")]
        Result<List<ST_DTO>> UpdateMany(List<ST> items, string dbName);
        [OperationContract(Name = "DeleteSprintTests")]
        Result<List<ST_DTO>> DeleteMany(List<ST> items, string dbName);
        [OperationContract(Name = "GetSprintTest")]
        Result<ST_DTO> Get(ST item, string dbName);
        [OperationContract(Name = "GetSprintTests")]
        Result<List<ST_DTO>> Gets(ST_DTO item, string dbName);
        [OperationContract(Name = "IsExistsSprintTest")]
        Result<ST_DTO> IsExists(ST item, string dbName);
        #endregion

        #region Physic Db
        [OperationContract(Name = "NewPhysicDb")]
        Result<PDB_DTO> New(PDB item, string dbName);
        [OperationContract(Name = "NewPhysicDbs")]
        Result<List<PDB_DTO>> NewMany(List<PDB> items, string dbName);
        [OperationContract(Name = "UpdatePhysicDb")]
        Result<PDB_DTO> Update(PDB item, string dbName);
        [OperationContract(Name = "UpdatePhysicDbs")]
        Result<List<PDB_DTO>> UpdateMany(List<PDB> items, string dbName);
        [OperationContract(Name = "DeletePhysicDbs")]
        Result<List<PDB_DTO>> DeleteMany(List<PDB> items, string dbName);
        [OperationContract(Name = "GetPhysicDb")]
        Result<PDB_DTO> Get(PDB item, string dbName);
        [OperationContract(Name = "GetPhysicDbs")]
        Result<List<PDB_DTO>> Gets(PDB item, string dbName);
        [OperationContract(Name = "IsExistsPhysicDb")]
        Result<PDB_DTO> IsExists(PDB item, string dbName);
        #endregion

        #region Physic Table
        [OperationContract(Name = "NewPhysicTable")]
        Result<PT_DTO> New(PT item, string dbName);
        [OperationContract(Name = "NewPhysicTables")]
        Result<List<PT_DTO>> NewMany(List<PT> items, string dbName);
        [OperationContract(Name = "UpdatePhysicTable")]
        Result<PT_DTO> Update(PT item, string dbName);
        [OperationContract(Name = "UpdatePhysicTables")]
        Result<List<PT_DTO>> UpdateMany(List<PT> items, string dbName);
        [OperationContract(Name = "DeletePhysicTables")]
        Result<List<PT_DTO>> DeleteMany(List<PT> items, string dbName);
        [OperationContract(Name = "GetPhysicTable")]
        Result<PT_DTO> Get(PT item, string dbName);
        [OperationContract(Name = "GetPhysicTables")]
        Result<List<PT_DTO>> Gets(PT item, string dbName);
        [OperationContract(Name = "IsExistsPhysicTable")]
        Result<PT_DTO> IsExists(PT item, string dbName);
        #endregion
    }
}
