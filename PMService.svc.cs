﻿using System.Collections.Generic;
using System.Linq;
using static PMRes;

namespace sopvn_wcf
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PMService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PMService.svc or PMService.svc.cs at the Solution Explorer and start debugging.
    public class PMService : IPMService
    {
        #region Hr
        public Result<HR_DTO> ChangeStatus(HR item)
        {
            var hasChange = HR_DAO.Instance.ChangeStatus(ref item);
            HR_DTO data = null;

            if (hasChange)
            {
                data = item.MapTo<HR, HR_DTO>();
                data.CreatedDateTimeStamp = data.CreatedDate.ToTimestamp();
                data.UpdatedDateTimeStamp = data.UpdatedDate.ToTimestamp();
                data.StatusType = HR_DAO.Instance.GetStatus(data.Status);
            }
            return new Result<HR_DTO>
            {
                Data = data,
                IsSuccess = hasChange,
                Mes = hasChange ? "Delete Employee Successfull." : "Delete Employee Fail."
            };
        }
        public Result<List<HR_DTO>> DeleteMany(List<HR> items)
        {
            var result = HR_DAO.Instance.DeleteMany(items);
            var check = result != null && result.Any();

            return new Result<List<HR_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = !check,
                Mes = check ? "Delete Employees Successfull." : "Delete Employees Fail."
            };
        }
        public Result<HR_DTO> Get(HR item)
        {
            var result = HR_DAO.Instance.Get(item.Id);
            var check = result != null;

            return new Result<HR_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? string.Empty : "None Data Found."
            };
        }
        public Result<List<HR_DTO>> Gets(HR item)
        {
            var result = HR_DAO.Instance.Gets(item);
            var check = result != null && result.Any();

            return new Result<List<HR_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ?  "None data found." :"Get data successfull."
            };
        }
        public Result<HR_DTO> New(HR item)
        {
            var result = HR_DAO.Instance.New(item);
            var check = result != null;

            return new Result<HR_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Create Employee Successfull." : "Create Employee Fail."
            };
        }
        public Result<List<HR_DTO>> NewMany(List<HR> items)
        {
            var result = HR_DAO.Instance.NewMany(items);
            var check = result != null && result.Any();

            return new Result<List<HR_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = !check,
                Mes = check ? "Insert Employees Successfull." : "Insert Employees Fail."
            };
        }
        public Result<HR_DTO> Update(HR item)
        {
            var result = HR_DAO.Instance.Update(item);
            var check = result != null;

            return new Result<HR_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Update Employee Successfull." : "Update Employee Fail."
            };
        }
        public Result<List<HR_DTO>> UpdateMany(List<HR> items)
        {
            var result = HR_DAO.Instance.UpdateMany(items);
            var check = result != null && result.Any();

            return new Result<List<HR_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Update Employees Successfull." : "Update Employees Fail."
            };
        }
        public Result<HR_DTO> IsExists(HR item)
        {
            var isExists = HR_DAO.Instance.IsExists(item.Email);

            return new Result<HR_DTO>
            {
                IsSuccess = isExists,
                Mes = isExists ? "Employee Already Exists." : ""
            };
        }

        public Result<HR_DTO> UpdateAvatar(HR item)
        {
            var result = HR_DAO.Instance.UpdateAvatar(item);
            var check = result != null;

            return new Result<HR_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Update Employee Avatar Successfull." : "Update Employee Avatar Fail."
            };
        }

        public Result<HR_DTO> UpdatePassword(HR item)
        {
            var result = HR_DAO.Instance.UpdatePassword(item);
            var check = result != null;

            return new Result<HR_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Update Employee Password Successfull." : "Update Employee Password Fail."
            };
        }
        #endregion

        #region Permision
        public Result<PER_DTO> ChangeStatus(PER item)
        {
            var hasChange = PER_DAO.Instance.ChangeStatus(ref item);
            PER_DTO data = null;

            if (hasChange)
            {
                data = item.MapTo<PER, PER_DTO>();
                data.CreatedDateTimeStamp = data.CreatedDate.ToTimestamp();
                data.UpdatedDateTimeStamp = data.UpdatedDate.ToTimestamp();
                data.StatusType = PER_DAO.Instance.GetStatus(data.Status);
            }
            return new Result<PER_DTO>
            {
                Data = data,
                IsSuccess = hasChange,
                Mes = hasChange ? "Delete Permision Successfull." : "Delete Permision Fail."
            };
        }
        public Result<List<PER_DTO>> DeleteMany(List<PER> items)
        {
            var result = PER_DAO.Instance.DeleteMany(items);
            var check = result != null && result.Any();

            return new Result<List<PER_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Delete Permisions Successfull." : "Delete Permisions Fail."
            };
        }
        public Result<PER_DTO> Get(PER item)
        {
            var result = PER_DAO.Instance.Get(item.Id);
            var check = result != null;

            return new Result<PER_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? string.Empty : "None Data Found."
            };
        }
        public Result<List<PER_DTO>> Gets(PER item)
        {
            var result = PER_DAO.Instance.Gets(item);
            var check = result.Any();

            return new Result<List<PER_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Get data Successfull." : "None data found."
            };
        }
        public Result<PER_DTO> New(PER item)
        {
            var result = PER_DAO.Instance.New(item);
            var check = result != null;

            return new Result<PER_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Create Permision Successfull." : "Create Permision Fail."
            };
        }
        public Result<List<PER_DTO>> NewMany(List<PER> items)
        {
            var results = PER_DAO.Instance.NewMany(items);
            var check = results != null && results.Any();

            return new Result<List<PER_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Insert Permisions Successfull." : "Insert Permisions Fail."
            };
        }
        public Result<PER_DTO> Update(PER item)
        {
            var result = PER_DAO.Instance.Update(item);
            var check = result != null;

            return new Result<PER_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Update Permision Successfull." : "Update Permision Fail."
            };
        }
        public Result<List<PER_DTO>> UpdateMany(List<PER> items)
        {
            var results = PER_DAO.Instance.UpdateMany(items);
            var check = results != null && results.Any();

            return new Result<List<PER_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Update Permisions Successfull." : "Update Permisions Fail."
            };
        }
        public Result<PER_DTO> IsExists(PER item)
        {
            var isExists = PER_DAO.Instance.IsExists(item.Name);

            return new Result<PER_DTO>
            {
                IsSuccess = isExists,
                Mes = isExists ? "Permision Already Exists." : ""
            };
        }
        #endregion

        #region Position
        public Result<POS_DTO> ChangeStatus(POS item)
        {
            var hasChange = POS_DAO.Instance.ChangeStatus(ref item);
            POS_DTO data = null;

            if (hasChange)
            {
                data = item.MapTo<POS, POS_DTO>();
                data.CreatedDateTimeStamp = data.CreatedDate.ToTimestamp();
                data.UpdatedDateTimeStamp = data.UpdatedDate.ToTimestamp();
                data.StatusType = POS_DAO.Instance.GetStatus(data.Status);
            }

            return new Result<POS_DTO>
            {
                Data = data,
                IsSuccess = hasChange,
                Mes = hasChange ? "Delete Position Successfull." : "Delete Position Fail."
            };
        }
        public Result<List<POS_DTO>> DeleteMany(List<POS> items)
        {
            var result = POS_DAO.Instance.DeleteMany(items);
            var check = result != null && result.Any();

            return new Result<List<POS_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Delete Positions Successfull." : "Delete Positions Fail."
            };
        }
        public Result<POS_DTO> Get(POS item)
        {
            var result = POS_DAO.Instance.Get(item.Id);
            var check = result != null;

            return new Result<POS_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? string.Empty : "None Data Found."
            };
        }
        public Result<List<POS_DTO>> Gets(POS item)
        {
            var result = POS_DAO.Instance.Gets(item);
            var check = result != null && result.Any();

            return new Result<List<POS_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Get data Successfull." : "None data Found."
            };
        }
        public Result<POS_DTO> New(POS item)
        {
            var result = POS_DAO.Instance.New(item);
            var check = result != null;

            return new Result<POS_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Create Position Successfull." : "Create Position Fail."
            };
        }
        public Result<List<POS_DTO>> NewMany(List<POS> items)
        {
            var results = POS_DAO.Instance.NewMany(items);
            var check = results != null && results.Any();

            return new Result<List<POS_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Insert Positions Successfull." : "Insert Positions Fail."
            };
        }
        public Result<POS_DTO> Update(POS item)
        {
            var result = POS_DAO.Instance.Update(item);
            var check = result != null;

            return new Result<POS_DTO>
            {
                Data = check ? result : result,
                IsSuccess = check,
                Mes = check ? "Update Position Successfull." : "Update Position Fail."
            };
        }
        public Result<List<POS_DTO>> UpdateMany(List<POS> items)
        {
            var results = POS_DAO.Instance.UpdateMany(items);
            var check = results.Any();

            return new Result<List<POS_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = !check,
                Mes = check ? "Update Positions Successfull." : "Update Positions Fail."
            };
        }
        public Result<POS_DTO> IsExists(POS item)
        {
            var isExists = POS_DAO.Instance.IsExists(item.Name);

            return new Result<POS_DTO>
            {
                IsSuccess = isExists,
                Mes = isExists ? "Position Already Exists." : ""
            };
        }
        #endregion

        #region Project
        public Result<PRO_DTO> ChangeStatus(PRO item)
        {
            var hasChange = PRO_DAO.Instance.ChangeStatus(item);
            PRO_DTO data = null;

            if (hasChange)
            {
                data = item.MapTo<PRO, PRO_DTO>();
                data.CreatedDateTimeStamp = data.CreatedDate.ToTimestamp();
                data.UpdatedDateTimeStamp = data.UpdatedDate.ToTimestamp();
                data.StatusType = PRO_DAO.Instance.GetStatus(data.Status);
            }
            return new Result<PRO_DTO>
            {
                Data = data,
                IsSuccess = hasChange,
                Mes = hasChange ? "Delete Project Successfull." : "Delete Project Fail."
            };
        }
        public Result<List<PRO_DTO>> DeleteMany(List<PRO> items)
        {
            var result = PRO_DAO.Instance.DeleteMany(items);
            var check = result != null && result.Any();

            return new Result<List<PRO_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Delete Projects Successfull." : "Delete Projects Fail."
            };
        }
        public Result<PRO_DTO> Get(PRO item)
        {
            var result = PRO_DAO.Instance.Get(item.Id);
            var check = result != null;

            return new Result<PRO_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? string.Empty : "None Data Fould."
            };
        }
        public Result<List<PRO_DTO>> Gets(PRO item)
        {
            var result = PRO_DAO.Instance.Gets(item);
            var check = result != null && result.Any();

            return new Result<List<PRO_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Get data Successfull." : "None data Found."
            };
        }
        public Result<PRO_DTO> New(PRO item)
        {
            var result = PRO_DAO.Instance.New(item);
            var check = result != null;

            return new Result<PRO_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Create Project Successfull." : "Create Project Success."
            };
        }
        public Result<List<PRO_DTO>> NewMany(List<PRO> items)
        {
            var results = PRO_DAO.Instance.NewMany(items);
            var check = results != null && results.Any();

            return new Result<List<PRO_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Insert Projects Successfull." : "Insert Projects Fail."
            };
        }
        public Result<PRO_DTO> Update(PRO item)
        {
            var result = PRO_DAO.Instance.Update(item);
            var check = result != null;

            return new Result<PRO_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Update Project Successfull." : "Update Project Fail."
            };
        }
        public Result<List<PRO_DTO>> UpdateMany(List<PRO> items)
        {
            var results = PRO_DAO.Instance.UpdateMany(items);
            var check = results != null && results.Any();

            return new Result<List<PRO_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Update Projects Successfull." : "Update Projects Fail."
            };
        }
        public Result<PRO_DTO> IsExists(PRO item)
        {
            var isExists = PRO_DAO.Instance.IsExists(item.Name);

            return new Result<PRO_DTO>
            {
                IsSuccess = isExists,
                Mes = isExists ? "Project Already Exists." : ""
            };
        }
        #endregion

        #region Project Permision
        public Result<PRO_PER_DTO> ChangeStatus(PRO_PER item)
        {
            var hasChange = PRO_PER_DAO.Instance.ChangeStatus(ref item);

            return new Result<PRO_PER_DTO>
            {
                IsSuccess = hasChange,
                Mes = hasChange ? "Delete Permision Successfull." : "Delete Permision Fail."
            };
        }
        public Result<List<PRO_PER_DTO>> DeleteMany(List<PRO_PER> items)
        {
            var result = PRO_PER_DAO.Instance.DeleteMany(items);
            var check = result != null && result.Any();

            return new Result<List<PRO_PER_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Delete Permision Successfull." : "Delete Permision Fail."
            };
        }
        public Result<PRO_PER_DTO> Get(PRO_PER item)
        {
            var result = PRO_PER_DAO.Instance.Get(item.Id);
            var check = result != null;

            return new Result<PRO_PER_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? string.Empty : "None Data Fould."
            };
        }
        public Result<List<PRO_PER_DTO>> Gets(PRO_PER_DTO item)
        {
            var result = PRO_PER_DAO.Instance.Gets(item);
            var check = result != null && result.Any();

            return new Result<List<PRO_PER_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Get data Successfull." : "None data Found."
            };
        }
        public Result<PRO_PER_DTO> New(PRO_PER item)
        {
            var result = PRO_PER_DAO.Instance.New(item);
            var check = result != null;

            return new Result<PRO_PER_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Create Permision Successfull." : "Create Permision Fail."
            };
        }
        public Result<List<PRO_PER_DTO>> NewMany(List<PRO_PER> items)
        {
            var results = PRO_PER_DAO.Instance.NewMany(items);
            var check = results != null && results.Any();

            return new Result<List<PRO_PER_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Insert Permision Successfull." : "Insert Permision Fail."
            };
        }
        public Result<PRO_PER_DTO> Update(PRO_PER item)
        {
            var result = PRO_PER_DAO.Instance.Update(item);
            var check = result != null;

            return new Result<PRO_PER_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Update Permision Successfull." : "Update Permision Fail."
            };
        }
        public Result<List<PRO_PER_DTO>> UpdateMany(List<PRO_PER> items)
        {
            var results = PRO_PER_DAO.Instance.UpdateMany(items);
            var check = results != null && results.Any();

            return new Result<List<PRO_PER_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Update Permision Successfull." : "Update Permision Fail."
            };
        }
        public Result<PRO_PER_DTO> IsExists(PRO_PER item)
        {
            var isExists = PRO_PER_DAO.Instance.IsExists(item);

            return new Result<PRO_PER_DTO>
            {
                IsSuccess = isExists,
                Mes = isExists ? "Permision Already Exists." : ""
            };
        }
        #endregion

        #region Project hr
        public Result<PRO_HR_DTO> ChangeStatus(PRO_HR item)
        {
            var hasChange = PRO_HR_DAO.Instance.ChangeStatus(ref item);

            return new Result<PRO_HR_DTO>
            {
                IsSuccess = hasChange,
                Mes = hasChange ? "Delete Employee Successfull." : "Delete Employee Fail."
            };
        }
        public Result<List<PRO_HR_DTO>> DeleteMany(List<PRO_HR> items)
        {
            var result = PRO_HR_DAO.Instance.DeleteMany(items);
            var check = result != null && result.Any();

            return new Result<List<PRO_HR_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Delete Employee Successfull." : "Delete Employee Fail."
            };
        }
        public Result<PRO_HR_DTO> Get(PRO_HR item)
        {
            var result = PRO_HR_DAO.Instance.Get(item.Id);
            var check = result != null;

            return new Result<PRO_HR_DTO>
            {
                Data = check ? result : result,
                IsSuccess = check,
                Mes = check ? string.Empty : "None Data Fould."
            };
        }
        public Result<List<PRO_HR_DTO>> Gets(PRO_HR_DTO item)
        {
            var result = PRO_HR_DAO.Instance.Gets(item);
            var check = result != null && result.Any();

            return new Result<List<PRO_HR_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Get data Successfull." : "None data Found."
            };
        }
        public Result<PRO_HR_DTO> New(PRO_HR item)
        {
            var result = PRO_HR_DAO.Instance.New(item);
            var check = result != null;

            return new Result<PRO_HR_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Create Employee Successfull." : "Create Employee Fail."
            };
        }
        public Result<List<PRO_HR_DTO>> NewMany(List<PRO_HR> items)
        {
            var results = PRO_HR_DAO.Instance.NewMany(items);
            var check = results != null && results.Any();

            return new Result<List<PRO_HR_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Insert Employee Successfull." : "Insert Employee Fail."
            };
        }
        public Result<PRO_HR_DTO> Update(PRO_HR item)
        {
            var result = PRO_HR_DAO.Instance.Update(item);
            var check = result != null;

            return new Result<PRO_HR_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Update Employee Successfull." : "Update Employee Fail."
            };
        }
        public Result<List<PRO_HR_DTO>> UpdateMany(List<PRO_HR> items)
        {
            var results = PRO_HR_DAO.Instance.UpdateMany(items);
            var check = results != null && results.Any();

            return new Result<List<PRO_HR_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Update Employee Successfull." : "Update Employee Fail."
            };
        }
        public Result<PRO_HR_DTO> IsExists(PRO_HR item)
        {
            var isExists = PRO_HR_DAO.Instance.IsExists(item);

            return new Result<PRO_HR_DTO>
            {
                IsSuccess = isExists,
                Mes = isExists ? "Employee Already Exists." : ""
            };
        }
        #endregion

        #region Requirement
        public Result<List<REQ_DTO>> DeleteMany(List<REQ> items, string dbName)
        {
            var result = REQ_DAO.Instance.DeleteMany(items, dbName);
            var check = result != null && result.Any();

            return new Result<List<REQ_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Delete Requirements Successfull." : "Delete Requirements Fail."
            };
        }
        public Result<REQ_DTO> Get(REQ item, string dbName)
        {
            var result = REQ_DAO.Instance.Get(item.Index, dbName);
            var check = result != null;

            return new Result<REQ_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? string.Empty : "None Data Fould."
            };
        }
        public Result<List<REQ_DTO>> Gets(REQ item, string dbName)
        {
            var result = REQ_DAO.Instance.Gets(item, dbName);
            var check = result != null && result.Any();

            return new Result<List<REQ_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? string.Empty : "None data Found."
            };
        }
        public Result<REQ_DTO> New(REQ item, string dbName)
        {
            var result = REQ_DAO.Instance.New(item, dbName);
            var check = result != null;

            return new Result<REQ_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Create Requirement Successfull." : "Create Requirement Fail."
            };
        }
        public Result<List<REQ_DTO>> NewMany(List<REQ> items, string dbName)
        {
            var results = REQ_DAO.Instance.NewMany(items, dbName);
            var check = results != null && results.Any();

            return new Result<List<REQ_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Insert Requirements Successfull." : "Insert Requirements Fail."
            };
        }
        public Result<REQ_DTO> Update(REQ item, string dbName)
        {
            var result = REQ_DAO.Instance.Update(item, dbName);
            var check = result != null;

            return new Result<REQ_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Update Requirement Successfull." : "Update Requirement Fail."
            };
        }
        public Result<List<REQ_DTO>> UpdateMany(List<REQ> items, string dbName)
        {
            var results = REQ_DAO.Instance.UpdateMany(items, dbName);
            var check = results != null && results.Any();

            return new Result<List<REQ_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Update Requirements Successfull." : "Update Requirements Fail."
            };
        }
        public Result<REQ_DTO> IsExists(REQ item, string dbName)
        {
            var isExists = REQ_DAO.Instance.IsExists(item, dbName);

            return new Result<REQ_DTO>
            {
                IsSuccess = isExists,
                Mes = isExists ? "Requirement Already Exists." : ""
            };
        }
        #endregion

        #region User Story
        public Result<List<US_DTO>> DeleteMany(List<US> items, string dbName)
        {
            var result = US_DAO.Instance.DeleteMany(items, dbName);
            var check = result != null && result.Any();

            return new Result<List<US_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Delete User Story Successfull." : "Delete User Story Fail."
            };
        }
        public Result<US_DTO> Get(US item, string dbName)
        {
            var result = US_DAO.Instance.Get(item.Index, dbName);
            var check = result != null;

            return new Result<US_DTO>
            {
                Data = check ? result : result,
                IsSuccess = check,
                Mes = check ? string.Empty : "None Data Fould."
            };
        }
        public Result<List<US_DTO>> Gets(US item, string dbName)
        {
            var result = US_DAO.Instance.Gets(item, dbName);
            var check = result != null && result.Any();

            return new Result<List<US_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? string.Empty : "None data found."
            };
        }
        public Result<US_DTO> New(US item, string dbName)
        {
            var result = US_DAO.Instance.New(item, dbName);
            var check = result != null;

            return new Result<US_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Create User Story Successfull." : "Create User Story Fail."
            };
        }
        public Result<List<US_DTO>> NewMany(List<US> items, string dbName)
        {
            var results = US_DAO.Instance.NewMany(items, dbName);
            var check = results != null && results.Any();

            return new Result<List<US_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Insert User Story Successfull." : "Insert User Story Fail."
            };
        }
        public Result<US_DTO> Update(US item, string dbName)
        {
            var result = US_DAO.Instance.Update(item, dbName);
            var check = result != null;

            return new Result<US_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Update User Story Successfull." : "Update User Story Fail."
            };
        }
        public Result<List<US_DTO>> UpdateMany(List<US> items, string dbName)
        {
            var results = US_DAO.Instance.UpdateMany(items, dbName);
            var check = results != null && results.Any();

            return new Result<List<US_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Update User Story Successfull." : "Update User Story Fail."
            };
        }
        public Result<US_DTO> IsExists(US item, string dbName)
        {
            var isExists = US_DAO.Instance.IsExists(item, dbName);

            return new Result<US_DTO>
            {
                IsSuccess = isExists,
                Mes = isExists ? "User Story Already Exists." : ""
            };
        }
        #endregion

        #region Sprint
        public Result<List<SPRINT_DTO>> DeleteMany(List<SPRINT> items, string dbName)
        {
            var result = SPRINT_DAO.Instance.DeleteMany(items, dbName);
            var check = result != null && result.Any();

            return new Result<List<SPRINT_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Delete Sprints Successfull." : "Delete Sprints Fail."
            };
        }
        public Result<SPRINT_DTO> Get(SPRINT item, string dbName)
        {
            var result = SPRINT_DAO.Instance.Get(item.Index, dbName);
            var check = result != null;

            return new Result<SPRINT_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? string.Empty : "None Data Fould."
            };
        }
        public Result<List<SPRINT_DTO>> Gets(SPRINT item, string dbName)
        {
            var result = SPRINT_DAO.Instance.Gets(item, dbName);
            var check = result != null && result.Any();

            return new Result<List<SPRINT_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? string.Empty : "None data found."
            };
        }
        public Result<SPRINT_DTO> New(SPRINT item, string dbName)
        {
            var result = SPRINT_DAO.Instance.New(item, dbName);
            var check = result != null;

            return new Result<SPRINT_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Create Sprint Successfull." : "Create Sprint Fail."
            };
        }
        public Result<List<SPRINT_DTO>> NewMany(List<SPRINT> items, string dbName)
        {
            var results = SPRINT_DAO.Instance.NewMany(items, dbName);
            var check = results != null && results.Any();

            return new Result<List<SPRINT_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Insert Sprints Successfull." : "Insert Sprints Fail."
            };
        }
        public Result<SPRINT_DTO> Update(SPRINT item, string dbName)
        {
            var result = SPRINT_DAO.Instance.Update(item, dbName);
            var check = result != null;

            return new Result<SPRINT_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Update Sprint Successfull." : "Update Sprint Fail."
            };
        }
        public Result<List<SPRINT_DTO>> UpdateMany(List<SPRINT> items, string dbName)
        {
            var results = SPRINT_DAO.Instance.UpdateMany(items, dbName);
            var check = results != null && results.Any();

            return new Result<List<SPRINT_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Update Sprints Successfull." : "Update Sprints Fail."
            };
        }
        public Result<SPRINT_DTO> IsExists(SPRINT item, string dbName)
        {
            var isExists = SPRINT_DAO.Instance.IsExists(item, dbName);

            return new Result<SPRINT_DTO>
            {
                IsSuccess = isExists,
                Mes = isExists ? "Sprint Already Exists." : ""
            };
        }
        #endregion

        #region Task
        public Result<List<TASK_DTO>> DeleteMany(List<TASK> items, string dbName)
        {
            var result = TASK_DAO.Instance.DeleteMany(items, dbName);
            var check = result != null && result.Any();

            return new Result<List<TASK_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Delete Tasks Successfull." : "Delete Tasks Fail."
            };
        }
        public Result<TASK_DTO> Get(TASK item, string dbName)
        {
            var result = TASK_DAO.Instance.Get(item.Index, dbName);
            var check = result != null;

            return new Result<TASK_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? string.Empty : "None Data Fould."
            };
        }
        public Result<List<TASK_DTO>> Gets(TASK_DTO item, string dbName)
        {
            var result = TASK_DAO.Instance.Gets(item, dbName);
            var check = result != null && result.Any();

            return new Result<List<TASK_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? string.Empty : "None data found."
            };
        }
        public Result<TASK_DTO> New(TASK item, string dbName)
        {
            var result = TASK_DAO.Instance.New(item, dbName);
            var check = result != null;

            return new Result<TASK_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Create Task Successfull." : "Create Task Fail."
            };
        }
        public Result<List<TASK_DTO>> NewMany(List<TASK> items, string dbName)
        {
            var results = TASK_DAO.Instance.NewMany(items, dbName);
            var check = results != null && results.Any();

            return new Result<List<TASK_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Insert Tasks Successfull." : "Insert Tasks Fail."
            };
        }
        public Result<TASK_DTO> Update(TASK item, string dbName)
        {
            var result = TASK_DAO.Instance.Update(item, dbName);
            var check = result != null;

            return new Result<TASK_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Update Task Successfull." : "Update Task Fail."
            };
        }
        public Result<List<TASK_DTO>> UpdateMany(List<TASK> items, string dbName)
        {
            var results = TASK_DAO.Instance.UpdateMany(items, dbName);
            var check = results != null && results.Any();

            return new Result<List<TASK_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Update Tasks Successfull." : "Update Tasks Fail."
            };
        }
        public Result<TASK_DTO> IsExists(TASK item, string dbName)
        {
            var isExists = TASK_DAO.Instance.IsExists(item, dbName);

            return new Result<TASK_DTO>
            {
                IsSuccess = isExists,
                Mes = isExists ? "Task Already Exists." : ""
            };
        }
        #endregion

        #region Sprint Test
        public Result<List<ST_DTO>> DeleteMany(List<ST> items, string dbName)
        {
            var result = ST_DAO.Instance.DeleteMany(items, dbName);
            var check = result != null && result.Any();

            return new Result<List<ST_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Delete Sprint Test Successfull." : "Delete Sprint Test Fail."
            };
        }
        public Result<ST_DTO> Get(ST item, string dbName)
        {
            var result = ST_DAO.Instance.Get(item.Index, dbName);
            var check = result != null;

            return new Result<ST_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? string.Empty : "None Data Fould."
            };
        }
        public Result<List<ST_DTO>> Gets(ST_DTO item, string dbName)
        {
            var result = ST_DAO.Instance.Gets(item, dbName);
            var check = result != null && result.Any();

            return new Result<List<ST_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? string.Empty : "None data found."
            };
        }
        public Result<ST_DTO> New(ST item, string dbName)
        {
            var result = ST_DAO.Instance.New(item, dbName);
            var check = result != null;

            return new Result<ST_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Create Sprint Test Successfull." : "Create Sprint Test Fail."
            };
        }
        public Result<List<ST_DTO>> NewMany(List<ST> items, string dbName)
        {
            var results = ST_DAO.Instance.NewMany(items, dbName);
            var check = results != null && results.Any();

            return new Result<List<ST_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Insert Sprint Test Successfull." : "Insert Sprint Test Fail."
            };
        }
        public Result<ST_DTO> Update(ST item, string dbName)
        {
            var result = ST_DAO.Instance.Update(item, dbName);
            var check = result == null;

            return new Result<ST_DTO>
            {
                Data = check ? null : result,
                IsSuccess = check,
                Mes = check ? "Update Sprint Test Fail." : "Update Sprint Test Successfull."
            };
        }
        public Result<List<ST_DTO>> UpdateMany(List<ST> items, string dbName)
        {
            var results = ST_DAO.Instance.UpdateMany(items, dbName);
            var check = results != null && results.Any();

            return new Result<List<ST_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Update Sprint Test Successfull." : "Update Sprint Test Fail."
            };
        }
        public Result<ST_DTO> IsExists(ST item, string dbName)
        {
            var isExists = ST_DAO.Instance.IsExists(item, dbName);

            return new Result<ST_DTO>
            {
                IsSuccess = isExists,
                Mes = isExists ? "Sprint Test Already Exists." : ""
            };
        }
        #endregion

        #region Physic Db
        public Result<List<PDB_DTO>> DeleteMany(List<PDB> items, string dbName)
        {
            var result = PDB_DAO.Instance.DeleteMany(items, dbName);
            var check = result != null && result.Any();

            return new Result<List<PDB_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Delete Physic Database Successfull." : "Delete Physic Database Fail."
            };
        }
        public Result<PDB_DTO> Get(PDB item, string dbName)
        {
            var result = PDB_DAO.Instance.Get(item.Index, dbName);
            var check = result != null;

            return new Result<PDB_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? string.Empty : "None Data Fould."
            };
        }
        public Result<List<PDB_DTO>> Gets(PDB item, string dbName)
        {
            var result = PDB_DAO.Instance.Gets(item, dbName);
            var check = result != null && result.Any();

            return new Result<List<PDB_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? string.Empty : "None data found."
            };
        }
        public Result<PDB_DTO> New(PDB item, string dbName)
        {
            var result = PDB_DAO.Instance.New(item, dbName);
            var check = result != null;

            return new Result<PDB_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Create Requirement Successfull." : "Create Requirement Fail."
            };
        }
        public Result<List<PDB_DTO>> NewMany(List<PDB> items, string dbName)
        {
            var results = PDB_DAO.Instance.NewMany(items, dbName);
            var check = results != null && results.Any();

            return new Result<List<PDB_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Insert Physic Database Successfull." : "Insert Physic Database Fail."
            };
        }
        public Result<PDB_DTO> Update(PDB item, string dbName)
        {
            var result = PDB_DAO.Instance.Update(item, dbName);
            var check = result != null;

            return new Result<PDB_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Update Requirement Successfull." : "Update Requirement Fail."
            };
        }
        public Result<List<PDB_DTO>> UpdateMany(List<PDB> items, string dbName)
        {
            var results = PDB_DAO.Instance.UpdateMany(items, dbName);
            var check = results != null && results.Any();

            return new Result<List<PDB_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Update Physic Database Successfull." : "Update Physic Database Fail."
            };
        }
        public Result<PDB_DTO> IsExists(PDB item, string dbName)
        {
            var isExists = PDB_DAO.Instance.IsExists(item, dbName);

            return new Result<PDB_DTO>
            {
                IsSuccess = isExists,
                Mes = isExists ? "Requirement Already Exists." : ""
            };
        }
        #endregion

        #region Physic Table
        public Result<List<PT_DTO>> DeleteMany(List<PT> items, string dbName)
        {
            var result = PT_DAO.Instance.DeleteMany(items, dbName);
            var check = result != null && result.Any();

            return new Result<List<PT_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Delete Physic Table Successfull." : "Delete Physic Table Fail."
            };
        }
        public Result<PT_DTO> Get(PT item, string dbName)
        {
            var result = PT_DAO.Instance.Get(item.Index, dbName);
            var check = result != null;

            return new Result<PT_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? string.Empty : "None Data Found."
            };
        }
        public Result<List<PT_DTO>> Gets(PT item, string dbName)
        {
            var result = PT_DAO.Instance.Gets(item, dbName);
            var check = result != null && result.Any();

            return new Result<List<PT_DTO>>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? string.Empty : "None data found."
            };
        }
        public Result<PT_DTO> New(PT item, string dbName)
        {
            var result = PT_DAO.Instance.New(item, dbName);
            var check = result != null;

            return new Result<PT_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Create Physic Table Successfull." : "Create Physic Table Fail."
            };
        }
        public Result<List<PT_DTO>> NewMany(List<PT> items, string dbName)
        {
            var results = PT_DAO.Instance.NewMany(items, dbName);
            var check = results != null && results.Any();

            return new Result<List<PT_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Insert Physic Table Successfull." : "Insert Physic Table Fail."
            };
        }
        public Result<PT_DTO> Update(PT item, string dbName)
        {
            var result = PT_DAO.Instance.Update(item, dbName);
            var check = result != null;

            return new Result<PT_DTO>
            {
                Data = check ? result : null,
                IsSuccess = check,
                Mes = check ? "Update Physic Table Successfull." : "Update Physic Table Fail."
            };
        }
        public Result<List<PT_DTO>> UpdateMany(List<PT> items, string dbName)
        {
            var results = PT_DAO.Instance.UpdateMany(items, dbName);
            var check = results != null && results.Any();

            return new Result<List<PT_DTO>>
            {
                Data = check ? results : null,
                IsSuccess = check,
                Mes = check ? "Update Physic Table Successfull." : "Update Physic Table Fail."
            };
        }
        public Result<PT_DTO> IsExists(PT item, string dbName)
        {
            var isExists = PT_DAO.Instance.IsExists(item, dbName);

            return new Result<PT_DTO>
            {
                IsSuccess = isExists,
                Mes = isExists ? "Physic Table Already Exists." : ""
            };
        }
        #endregion
    }
}
