﻿using System;

/// <summary>
/// Summary description for TimeExtention
/// </summary>
public static class TimeExtention
{
    public static double ToTimestamp(this DateTime dateTime)
    {
        var dateDefault = new DateTime(1970, 1, 1, 0, 0, 0);
        var timeSpan = dateTime - dateDefault;
        return (long)timeSpan.TotalSeconds;
    }
    public static DateTime ToDateTime(this double timestamp)
    {
        if (timestamp <= 0)
        {
            return default(DateTime);
        }

        DateTime dateDefault = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
        var dtDateTime = dateDefault.AddSeconds(timestamp);
        return dtDateTime;
    }
}