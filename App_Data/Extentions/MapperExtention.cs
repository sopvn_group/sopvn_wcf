﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

/// <summary>
/// Summary description for MapperExtention
/// </summary>
public static class MapperExtention
{
    public static List<T> ToList<T>(this DataTable table)
    {
        var objList = new List<T>();
        if (table == null)
            return objList;

        var props = typeof(T).GetRuntimeProperties().ToList();
        var columns = table.Columns;
        var index = columns.Count;

        foreach (DataRow row in table.Rows)
        {
            var obj = Activator.CreateInstance<T>();
            
            for (int i = 0; i < index; i++)
            {
                var value = row[i];
                var rowType = row[i].GetType().Name;
                var columnName = columns[i].ColumnName;
                foreach (var prop in props)
                {
                    if (prop.PropertyType.Name.ToLower().Equals(rowType.ToLower()) && prop.Name.ToLower().Equals(columnName.ToLower()))
                        prop.SetValue(obj, value == DBNull.Value ? null : value);
                }

            }
            objList.Add(obj);
        }

        return objList;
    }

    public static K Map<T, K>(this T data)
    {
        var kProps = typeof(K).GetRuntimeProperties().ToList();
        var tProps = typeof(T).GetRuntimeProperties().ToList();

        var obj = Activator.CreateInstance<K>();
        foreach (var tProp in tProps)
        {
            var value = tProp.GetValue(data);
            var type = tProp.PropertyType.Name;
            var tPropName = tProp.Name;
            foreach (var kProp in kProps)
            {
                if (kProp.PropertyType.Name.ToLower().Equals(type.ToLower()) && kProp.Name.ToLower().Equals(tPropName.ToLower()))
                    kProp.SetValue(obj, value == DBNull.Value ? null : value);
            }
        }

        return obj;
    }

    public static List<K> Map<T, K>(this List<T> data)
    {
        var objList = new List<K>();

        foreach (var item in data)
        {
            var obj = item.Map<T, K>();
            objList.Add(obj);
        }

        return objList;
    }
}