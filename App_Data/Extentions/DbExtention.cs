﻿using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using static PMRes;

/// <summary>
/// Summary description for DbExtention
/// </summary>
public static class DbExtention
{
    public static void Exec(this SqlCommand cmd, Action<DataSet> handleResults, bool manageConnection = true)
    {
        try
        {
            if (manageConnection && cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.SelectCommand = cmd;

            DataSet dataset = new DataSet();
            da.Fill(dataset);
            handleResults(dataset);

            cmd.Connection.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static DataTable Exec(this SqlCommand cmd, bool manageConnection = true)
    {
        try
        {
            if (manageConnection && cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.SelectCommand = cmd;
            DataSet dataset = new DataSet();
            da.Fill(dataset);
            cmd.Connection.Close();
            return dataset.Tables[0];
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void ExecNone(this SqlCommand cmd, bool manageConnection = true)
    {
        try
        {
            if (manageConnection && cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();

            var result = cmd.ExecuteNonQuery();
            cmd.Connection.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void Exec(this SqlCommand cmd, Action<int> handleResults, bool manageConnection = true)
    {
        try
        {
            if (manageConnection && cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();

            var result = cmd.ExecuteNonQuery();
            handleResults(result);
            cmd.Connection.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static SqlCommand AddParam(this SqlCommand cmd, string paramName, object paramValue)
    {
        var param = cmd.CreateParameter();
        param.ParameterName = paramName;
        param.Value = paramValue != null ? paramValue : DBNull.Value;
        cmd.Parameters.Add(param);
        return cmd;
    }

    public static List<T> ToList<T>(this IMongoCollection<T> collection)
    {
        IMongoQueryable<T> results = from data in collection.AsQueryable() select data;

        var converted = new List<T>();
        foreach (T result in results)
            converted.Add(result);

        return converted;
    }
}