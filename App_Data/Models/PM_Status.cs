﻿using System.Runtime.Serialization;

/// <summary>
/// Summary description for Pm_Status
/// </summary>
public partial class PMRes
{
    [DataContract]
    public class StatusType
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string BadgeCss { get; set; }
        [DataMember]
        public bool IsHidden { get; set; }
        public StatusType()
        {

        }
        public StatusType(string Name, string BadgeCss, bool IsHidden = false)
        {
            this.Name = Name;
            this.BadgeCss = BadgeCss;
            this.IsHidden = IsHidden;
        }
    }
}