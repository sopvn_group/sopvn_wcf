﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

public sealed partial class PMRes
{
    [DataContract(Name = "Requirement")]
    public sealed class REQ
    {
       [DataMember] public ObjectId _id { get; set; }
       [DataMember] public int Index { get; set; }
       [DataMember] public int Rank { get; set; }
       [DataMember] public string Title { get; set; }
       [DataMember] public string Description { get; set; }
       [DataMember] public int ParentId { get; set; }
       [DataMember] public DateTime? CreatedDate { get; set; }
       [DataMember] public DateTime? UpdatedDate { get; set; }
       [DataMember] public bool? Status { get; set; }
       [DataMember] public List<int> UserStoryIds { get; set; }
    }

    [DataContract(Name = "RequirementDTO")]
    public sealed class REQ_DTO
    {
        [DataMember] public ObjectId _id { get; set; }
        [DataMember] public int Index { get; set; }
        [DataMember] public int Rank { get; set; }
        [DataMember] public string Title { get; set; }
        [DataMember] public string Description { get; set; }
        [DataMember] public int ParentId { get; set; }
        [DataMember] public DateTime CreatedDate { get; set; }
        [DataMember] public DateTime UpdatedDate { get; set; }
        [DataMember] public double CreatedDateTimeStamp { get; set; }
        [DataMember] public double UpdatedDateTimeStamp { get; set; }
        [DataMember] public bool? Status { get; set; }
        [DataMember] public StatusType StatusType { get; set; }
        [DataMember] public List<int> UserStoryIds { get; set; }
    }

    public sealed class REQ_DAO
    {
        private static REQ_DAO instance = null;
        public static REQ_DAO Instance
        {
            get
            {
                instance = instance ?? new REQ_DAO();
                return instance;
            }
        }
        private REQ_DAO() { }
        public REQ_DTO New(REQ newItem,string dbName)
        {
            try
            {
                REQ_DTO inserted = null;

                if (IsExists(newItem, dbName))
                    return inserted;

                Db.Instance.GetCollection<REQ>(dbName, Table_Requirement).InsertOneAsync(newItem);
                
                inserted = newItem.Map<REQ,REQ_DTO>();

                FillJson(inserted);

                return inserted.Index > 0 ? inserted : null;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public List<REQ_DTO> NewMany(List<REQ> newItem, string dbName)
        {
            var fails = new List<REQ>();
            var success = new List<REQ_DTO>();
            REQ currentReq = null;
            try
            {
                foreach (var item in newItem)
                {
                    currentReq = item;
                    var inserted = New(item, dbName);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }
            }
            catch (Exception)
            {
                fails.Add(currentReq);
            }
            FillJson(success);
            return success;
        }
        public bool IsExists(REQ item, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<REQ>(dbName, Table_Requirement)
                                        .Find(x => x.Title.Equals(item.Title) && x.Index == item.Index);
                
                return result.Any();
            }
            catch (Exception)
            {
                return true;
            }
        }
        public REQ_DTO Update(REQ item, string dbName)
        {
            try
            {
                REQ_DTO inserted = null;
                var check = Db.Instance.GetCollection<REQ>(dbName, Table_Requirement)
                                        .Find(x => x.Title.Equals(item.Title));
                if (check.Any())
                    return inserted;

                Db.Instance.GetCollection<REQ>(dbName, Table_Requirement).ReplaceOne(x => x.Index == item.Index,item);
                inserted = item.Map<REQ, REQ_DTO>();
                FillJson(inserted);

                return inserted.Index > 0 ? inserted : null;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<REQ_DTO> UpdateMany(List<REQ> items, string dbName)
        {
            var fails = new List<REQ>();
            var success = new List<REQ_DTO>();
            REQ currentReq = null;
            try
            {
                foreach (var item in items)
                {
                    currentReq = item;
                    var inserted = Update(item, dbName);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }

            }
            catch (Exception)
            {
                fails.Add(currentReq);
            }
            FillJson(success);
            return success;
        }
        public REQ_DTO Get(int id, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<REQ>(dbName, Table_Requirement)
                                        .Find(x => x.Index == id);
                REQ_DTO inserted = null;

                if (result.Any())
                {
                    inserted = result.FirstOrDefault().Map<REQ,REQ_DTO>();
                    FillJson(inserted);
                }

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<REQ_DTO> Gets(REQ item, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<REQ_DTO>(dbName, Table_Requirement).ToList();

                var filtered = result.Where(x => (item.Title is null || item.Title.Equals(string.Empty) || x.Title.Equals(item.Title))
                                                && (item.Rank == 0 || x.Rank == item.Rank)
                                                && (item.CreatedDate is null || x.CreatedDate == item.CreatedDate)
                                                && (item.UpdatedDate is null || x.UpdatedDate == item.UpdatedDate)
                                                && (item.Status is null || x.Status == item.Status));
                if (filtered != null && filtered.Any())
                    FillJson(filtered.ToList());

                return filtered.ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<REQ_DTO> DeleteMany(List<REQ> items, string dbName)
        {
            var fails = new List<REQ>();
            var success = new List<REQ_DTO>();
            var currentItem = items.FirstOrDefault();
            try
            {
                foreach (var item in items)
                {
                    currentItem = item;
                    var updated = Update(currentItem, dbName);
                    if (updated is null)
                        fails.Add(item);
                    else
                        success.Add(item.Map<REQ, REQ_DTO>());
                }
            }
            catch (Exception)
            {
                fails.Add(currentItem);
            }
            FillJson(success);
            return success;
        }
        private void FillJson(REQ_DTO inserted)
        {
            inserted.CreatedDateTimeStamp = inserted.CreatedDate.ToTimestamp();
            inserted.UpdatedDateTimeStamp = inserted.UpdatedDate.ToTimestamp();
            inserted.StatusType = GetStatus(inserted.Status);
            
        }
        private void FillJson(List<REQ_DTO> inserteds)
        {
            if (inserteds != null)
            {
                foreach (var inserted in inserteds)
                {
                    FillJson(inserted);
                }
            }
        }
        private StatusType GetStatus(bool? status)
        {
            switch (status)
            {
                case true:
                    return new StatusType("Active", "badge bg-success", true);
                case false:
                default:
                    return new StatusType("Inactive", "badge bg-danger");
            }
        }
    }
}