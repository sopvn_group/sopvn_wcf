﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for PM_Project
/// </summary>
public partial class PMRes
{
    [DataContract(Name = "Position")]
    public sealed class POS
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime UpdatedDate { get; set; }
        [DataMember]
        public bool? Status { get; set; }
    }
    [DataContract(Name = "PositionDTO")]
    public class POS_DTO
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime UpdatedDate { get; set; }
        [DataMember]
        public double CreatedDateTimeStamp { get; set; }
        [DataMember]
        public double UpdatedDateTimeStamp { get; set; }
        [DataMember]
        public bool Status { get; set; }
        [DataMember]
        public StatusType StatusType { get; set; }
    }
    public sealed class POS_DAO
    {
        private static POS_DAO instance = null;
        public static POS_DAO Instance
        {
            get
            {
                instance = instance ?? new POS_DAO();
                return instance;
            }
        }
        private POS_DAO() { }
        public POS_DTO New(POS newItem)
        {
            try
            {
                POS_DTO inserted = null;

                if (IsExists(newItem.Name))
                    return inserted;

                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Position_Insert)
                                           .AddParam("@Name", newItem.Name)
                                           .AddParam("@CreatedDate", DateTime.UtcNow)
                                           .Exec().ToList<POS_DTO>();

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public List<POS_DTO> NewMany(List<POS> newItem)
        {
            var fails = new List<POS>();
            var success = new List<POS_DTO>();
            POS currentPos = null;
            try
            {
                foreach (var item in newItem)
                {
                    currentPos = item;
                    var inserted = New(item);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }
            }
            catch (Exception)
            {
                fails.Add(currentPos);
            }

            return success;
        }
        public bool IsExists(string name)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Position_IsExists)
                                           .AddParam("@Name", name)
                                           .Exec().ToList<POS>();

                return inserteds.Any();
            }
            catch (Exception)
            {
                return true;
            }
        }
        public POS_DTO Update(POS item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Position_Update)
                                           .AddParam("@Name", item.Name)
                                           .AddParam("@UpdatedDate", DateTime.UtcNow)
                                           .AddParam("@Status", item.Status)
                                           .Exec().ToList<POS_DTO>();
                POS_DTO inserted = null;

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<POS_DTO> UpdateMany(List<POS> items)
        {
            var fails = new List<POS>();
            var success = new List<POS_DTO>();
            POS currentPos = null;
            try
            {
                foreach (var item in items)
                {
                    currentPos = item;
                    var inserted = Update(item);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }

            }
            catch (Exception)
            {
                fails.Add(currentPos);
            }

            return success;
        }
        public POS_DTO Get(int id)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Position_GetById)
                                           .AddParam("@Id", id)
                                           .Exec().ToList<POS_DTO>();
                POS_DTO inserted = null;

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<POS_DTO> Gets(POS item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Position_Gets)
                                           .AddParam("@Name", item.Name)
                                           .AddParam("@Status", item.Status)
                                           .Exec().ToList<POS_DTO>();

                FillJson(inserteds);

                return inserteds;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool ChangeStatus(ref POS item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Position_ChangeStatus)
                                           .AddParam("@Id", item.Id)
                                           .AddParam("@UpdatedDate", DateTime.UtcNow)
                                           .AddParam("@Status", item.Status)
                                           .Exec().ToList<POS>();
                var check = inserteds.Any();
                if (check)
                    item = inserteds.FirstOrDefault();

                return check;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public List<POS_DTO> DeleteMany(List<POS> items)
        {
            var fails = new List<POS>();
            var success = new List<POS_DTO>();
            var currentItem = items.FirstOrDefault();
            try
            {
                foreach (var item in items)
                {
                    currentItem = item;
                    if (!ChangeStatus(ref currentItem))
                        fails.Add(item);
                    else
                        success.Add(item.Map<POS, POS_DTO>());
                }
            }
            catch (Exception)
            {
                fails.Add(currentItem);
            }
            FillJson(success);
            return success;
        }
        private void FillJson(List<POS_DTO> inserteds)
        {
            if (inserteds != null)
            {
                foreach (var inserted in inserteds)
                {
                    inserted.CreatedDateTimeStamp = inserted.CreatedDate.ToTimestamp();
                    inserted.UpdatedDateTimeStamp = inserted.UpdatedDate.ToTimestamp();
                    inserted.StatusType = GetStatus(inserted.Status);
                }
            }
        }
        public StatusType GetStatus(bool status)
        {
            switch (status)
            {
                case true:
                    return new StatusType("Active", "badge bg-success", true);
                case false:
                default:
                    return new StatusType("Inactive", "badge bg-danger");
            }
        }
    }
}