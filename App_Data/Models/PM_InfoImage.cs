﻿public sealed partial class PMRes
{
    public sealed class II
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public int Type { get; set; }
        public bool Status { get; set; }
    }

    public sealed class II_DAO
    {
        private static II_DAO instance = null;
        public static II_DAO Instance
        {
            get
            {
                instance = instance ?? new II_DAO();
                return instance;
            }
        }
        private II_DAO() { }
    }
}