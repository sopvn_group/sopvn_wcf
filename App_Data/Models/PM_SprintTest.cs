﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;

public sealed partial class PMRes
{
    [DataContract(Name = "SprintTest")]
    public sealed class ST
    {
       [DataMember] public ObjectId _id { get; set; }
       [DataMember] public int Index { get; set; }
       [DataMember] public int SprintId { get; set; }
       [DataMember] public int TestId { get; set; }
       [DataMember] public string TestCase { get; set; }
       [DataMember] public DateTime CreatedDate { get; set; }
       [DataMember] public DateTime UpdatedDate { get; set; }
       [DataMember] public int Status { get; set; }
    }
    [DataContract(Name = "SprintTestDTO")]
    public sealed class ST_DTO
    {
       [DataMember] public ObjectId _id { get; set; }
       [DataMember] public int Index { get; set; }
       [DataMember] public int SprintId { get; set; }
       [DataMember] public string SprintTitle { get; set; }
       [DataMember] public int TestId { get; set; }
       [DataMember] public string TestName { get; set; }
       [DataMember] public string TestCase { get; set; }
       [DataMember] public DateTime CreatedDate { get; set; }
       [DataMember] public DateTime UpdatedDate { get; set; }
       [DataMember] public double CreatedDateTimeStamp { get; set; }
       [DataMember] public double UpdatedDateTimeStamp { get; set; }
       [DataMember] public int Status { get; set; }
       [DataMember] public StatusType StatusType { get; set; }
    }
    public sealed class ST_DAO
    {
        private static ST_DAO instance = null;
        public static ST_DAO Instance
        {
            get
            {
                instance = instance ?? new ST_DAO();
                return instance;
            }
        }
        private ST_DAO() { }
        public ST_DTO New(ST newItem, string dbName)
        {
            try
            {
                ST_DTO inserted = null;

                if (IsExists(newItem, dbName))
                    return inserted;

                Db.Instance.GetCollection<ST>(dbName, Table_SprintTest).InsertOneAsync(newItem);

                inserted = newItem.Map<ST, ST_DTO>();

                FillJson(inserted);

                return inserted.Index > 0 ? inserted : null;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public List<ST_DTO> NewMany(List<ST> newItem, string dbName)
        {
            var fails = new List<ST>();
            var success = new List<ST_DTO>();
            ST currentST = null;
            try
            {
                foreach (var item in newItem)
                {
                    currentST = item;
                    var inserted = New(item, dbName);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }
            }
            catch (Exception)
            {
                fails.Add(currentST);
            }
            FillJson(success);
            return success;
        }
        public bool IsExists(ST item, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<ST>(dbName, Table_SprintTest)
                                        .Find(x => x.Index == item.Index);

                return result.Any();
            }
            catch (Exception)
            {
                return true;
            }
        }
        public ST_DTO Update(ST item, string dbName)
        {
            try
            {
                ST_DTO inserted = null;
                var check = Db.Instance.GetCollection<ST>(dbName, Table_SprintTest)
                                        .Find(x => x.Index == item.Index);
                if (check.Any())
                    return inserted;

                Db.Instance.GetCollection<ST>(dbName, Table_SprintTest).ReplaceOne(x => x.Index == item.Index, item);
                inserted = item.Map<ST, ST_DTO>();
                FillJson(inserted);

                return inserted.Index > 0 ? inserted : null;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<ST_DTO> UpdateMany(List<ST> items, string dbName)
        {
            var fails = new List<ST>();
            var success = new List<ST_DTO>();
            ST currentST = null;
            try
            {
                foreach (var item in items)
                {
                    currentST = item;
                    var inserted = Update(item, dbName);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }

            }
            catch (Exception)
            {
                fails.Add(currentST);
            }
            FillJson(success);
            return success;
        }
        public ST_DTO Get(int id, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<ST>(dbName, Table_SprintTest)
                                        .Find(x => x.Index == id);
                ST_DTO inserted = null;

                if (result.Any())
                {
                    inserted = result.FirstOrDefault().Map<ST, ST_DTO>();
                    FillJson(inserted);
                }

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<ST_DTO> Gets(ST_DTO item, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<ST_DTO>(dbName, Table_SprintTest).ToList();

                var filtered = result.Where(x => (item.SprintTitle is null || item.SprintTitle.Equals(string.Empty) || x.SprintTitle.Equals(item.SprintTitle))
                                                && (item.TestName is null || item.TestName.Equals(string.Empty) || x.TestName.Equals(item.TestName))
                                                && (item.CreatedDate == null || x.CreatedDate == item.CreatedDate)
                                                && (item.UpdatedDate == null || x.UpdatedDate == item.UpdatedDate)
                                                && (item.Status == 0 || x.Status == item.Status));
                if (filtered != null && filtered.Any())
                    FillJson(filtered.ToList());

                return filtered.ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<ST_DTO> DeleteMany(List<ST> items, string dbName)
        {
            var fails = new List<ST>();
            var success = new List<ST_DTO>();
            var currentItem = items.FirstOrDefault();
            try
            {
                foreach (var item in items)
                {
                    currentItem = item;
                    var updated = Update(currentItem, dbName);
                    if (updated is null)
                        fails.Add(item);
                    else
                        success.Add(item.Map<ST, ST_DTO>());
                }
            }
            catch (Exception)
            {
                fails.Add(currentItem);
            }
            FillJson(success);
            return success;
        }
        private void FillJson(ST_DTO inserted)
        {
            inserted.CreatedDateTimeStamp = inserted.CreatedDate.ToTimestamp();
            inserted.UpdatedDateTimeStamp = inserted.UpdatedDate.ToTimestamp();
            inserted.StatusType = GetStatus(inserted.Status);

        }
        private void FillJson(List<ST_DTO> inserteds)
        {
            if (inserteds != null)
            {
                foreach (var inserted in inserteds)
                {
                    FillJson(inserted);
                }
            }
        }
        private StatusType GetStatus(int? status)
        {
            switch (status)
            {
                case 1:
                    return new StatusType("Active", "badge bg-success", true);
                case 2:
                default:
                    return new StatusType("Inactive", "badge bg-danger");
            }
        }
    }
}