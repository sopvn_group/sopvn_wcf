﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;

/// <summary>
/// Summary description for PM_PRO
/// </summary>
public partial class PMRes
{
    [DataContract(Name = "Project")]
    public sealed class PRO
    {
        [DataMember] public int Id { get; set; }
        [DataMember] public string Name { get; set; }
        [DataMember] public string Document { get; set; }
        [DataMember] public string Scrum { get; set; }
        [DataMember] public int TotalTime { get; set; }
        [DataMember] public decimal Cost { get; set; }
        [DataMember] public string Avatar { get; set; }
        [DataMember] public string ImageInfo { get; set; }
        [DataMember] public int Status { get; set; }
        [DataMember] public string Connection { get; set; }
        [DataMember] public string Database { get; set; }
        [DataMember] public string History { get; set; }
        [DataMember] public DateTime CreatedDate { get; set; }
        [DataMember] public DateTime UpdatedDate { get; set; }
    }
    [DataContract(Name = "ProjectDTO")]
    public sealed class PRO_DTO
    {
        [DataMember] public int Id { get; set; }
        [DataMember] public string Name { get; set; }
        [DataMember] public string Document { get; set; }
        [DataMember] public string Scrum { get; set; }
        [DataMember] public int TotalTime { get; set; }
        [DataMember] public decimal Cost { get; set; }
        [DataMember] public string Avatar { get; set; }
        [DataMember] public string ImageInfo { get; set; }
        [DataMember] public string Connection { get; set; }
        [DataMember] public string Database { get; set; }
        [DataMember] public DateTime CreatedDate { get; set; }
        [DataMember] public DateTime UpdatedDate { get; set; }
        [DataMember] public double CreatedDateTimeStamp { get; set; }
        [DataMember] public double UpdatedDateTimeStamp { get; set; }
        [DataMember] public string History { get; set; }
        [DataMember] public int Status { get; set; }
        [DataMember] public StatusType StatusType { get; set; }
    }
    public class PRO_IMAGE
    {
        public int Id { get; set; }
        public string Path { get; set; }
        public int Type { get; set; }
    }
    public sealed class PRO_DAO
    {
        private static PRO_DAO instance = null;
        public static PRO_DAO Instance
        {
            get
            {
                instance = instance ?? new PRO_DAO();
                return instance;
            }
        }
        private PRO_DAO() { }
        public PRO_DTO New(PRO newItem)
        {
            try
            {
                PRO_DTO inserted = null;

                if (IsExists(newItem.Name))
                    return inserted;

                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Project_Insert)
                                           .AddParam("@Name", newItem.Name)
                                           .AddParam("@Document", newItem.Document)
                                           .AddParam("@Scrum", newItem.Scrum)
                                           .AddParam("@Avatar", newItem.Avatar)
                                           .AddParam("@Connection", newItem.Connection)
                                           .AddParam("@Database", newItem.Database)
                                           .AddParam("@CreatedDate", DateTime.UtcNow)
                                           .Exec().ToList<PRO_DTO>();

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public List<PRO_DTO> NewMany(List<PRO> newItem)
        {
            var fails = new List<PRO>();
            var success = new List<PRO_DTO>();
            PRO currentPRO = null;
            try
            {
                foreach (var item in newItem)
                {
                    currentPRO = item;
                    var inserted = New(item);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }
            }
            catch (Exception)
            {
                fails.Add(currentPRO);
            }

            return success;
        }
        public bool IsExists(string name)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Project_IsExists)
                                           .AddParam("@Name", name)
                                           .Exec().ToList<PRO>();

                return inserteds.Any();
            }
            catch (Exception)
            {
                return true;
            }
        }
        public PRO_DTO UpdateAvatar(PRO item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Project_UpdateAvatar)
                                           .AddParam("@Id", item.Id)
                                           .AddParam("@Avatar", item.Avatar)
                                           .AddParam("@UpdatedDate", DateTime.UtcNow)
                                           .Exec().ToList<PRO_DTO>();
                PRO_DTO inserted = null;

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public PRO_DTO Update(PRO item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Project_Update)
                                           .AddParam("@Id", item.Id)
                                           .AddParam("@Name", item.Name)
                                           .AddParam("@Document", item.Document)
                                           .AddParam("@Scrum", item.Scrum)
                                           .AddParam("@Connection", item.Connection)
                                           .AddParam("@Database", item.Database)
                                           .AddParam("@CreatedDate", DateTime.UtcNow)
                                           .Exec().ToList<PRO_DTO>();
                PRO_DTO inserted = null;

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<PRO_DTO> UpdateMany(List<PRO> items)
        {
            var fails = new List<PRO>();
            var success = new List<PRO_DTO>();
            PRO currentPRO = null;
            try
            {
                foreach (var item in items)
                {
                    currentPRO = item;
                    var inserted = Update(item);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }

            }
            catch (Exception)
            {
                fails.Add(currentPRO);
            }

            return success;
        }
        public PRO_DTO Get(int id)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Project_GetById)
                                           .AddParam("@Id", id)
                                           .Exec().ToList<PRO_DTO>();
                PRO_DTO inserted = null;

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<PRO_DTO> Gets(PRO item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Project_Gets)
                                           .AddParam("@Name", item.Name)
                                           .AddParam("@Status", item.Status)
                                           .Exec().ToList<PRO_DTO>();
                FillJson(inserteds);

                return inserteds;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool ChangeStatus(PRO item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Project_ChangeStatus)
                                           .AddParam("@Id", item.Id)
                                           .AddParam("@UpdatedDate", DateTime.UtcNow)
                                           .AddParam("@Status", item.Status)
                                           .Exec().ToList<PRO_DTO>();
                return inserteds.Any();
            }
            catch (Exception)
            {
                return false;
            }
        }
        public List<PRO_DTO> DeleteMany(List<PRO> items)
        {
            var fails = new List<PRO>();
            var success = new List<PRO_DTO>();
            var currentItem = items.FirstOrDefault();
            try
            {
                foreach (var item in items)
                {
                    currentItem = item;
                    if (!ChangeStatus(item))
                        fails.Add(item);
                    else
                        success.Add(item.Map<PRO, PRO_DTO>());
                }
            }
            catch (Exception)
            {
                fails.Add(currentItem);
            }
            FillJson(success);
            return success;
        }
        private void FillJson(List<PRO_DTO> inserteds)
        {
            if (inserteds != null)
            {
                foreach (var inserted in inserteds)
                {
                    inserted.CreatedDateTimeStamp = inserted.CreatedDate.ToTimestamp();
                    inserted.UpdatedDateTimeStamp = inserted.UpdatedDate.ToTimestamp();
                    inserted.StatusType = GetStatus(inserted.Status);
                }
            }
        }
        public StatusType GetStatus(int status)
        {
            switch (status)
            {
                case 1:
                    return new StatusType("In Process", "badge bg-success", true);
                case 2:
                    return new StatusType("Pending", "badge bg-warning");
                case 3:
                    return new StatusType("Complete", "badge bg-success");
                case 4:
                default:
                    return new StatusType("Inactive", "badge bg-danger");
            }
        }
    }
}