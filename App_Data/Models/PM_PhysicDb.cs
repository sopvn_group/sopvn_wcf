﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

public sealed partial class PMRes
{
    [DataContract(Name = "PhysicDatabase")]
    public sealed class PDB
    {
        [DataMember] public ObjectId _id { get; set; }
        [DataMember] public int Index { get; set; }
        [DataMember] public string Name { get; set; }
        [DataMember] public DateTime? CreatedDate { get; set; }
        [DataMember] public DateTime? UpdatedDate { get; set; }
        [DataMember] public bool? Status { get; set; }
        [DataMember] public List<int> TableIds { get; set; }
    }
    [DataContract(Name = "PhysicDatabaseDTO")]
    public sealed class PDB_DTO
    {
        [DataMember] public ObjectId _id { get; set; }
        [DataMember] public int Index { get; set; }
        [DataMember] public string Name { get; set; }
        [DataMember] public DateTime CreatedDate { get; set; }
        [DataMember] public DateTime UpdatedDate { get; set; }
        [DataMember] public double CreatedDateTimeStamp { get; set; }
        [DataMember] public double UpdatedDateTimeStamp { get; set; }
        [DataMember] public bool? Status { get; set; }
        [DataMember] public StatusType StatusType { get; set; }
        [DataMember] public List<int> TableIds { get; set; }
    }
    public sealed class PDB_DAO
    {
        private static PDB_DAO instance = null;
        public static PDB_DAO Instance
        {
            get
            {
                instance = instance ?? new PDB_DAO();
                return instance;
            }
        }
        private PDB_DAO() { }
        public PDB_DTO New(PDB newItem, string dbName)
        {
            try
            {
                PDB_DTO inserted = null;

                if (IsExists(newItem, dbName))
                    return inserted;

                Db.Instance.GetCollection<PDB>(dbName, Table_Requirement).InsertOneAsync(newItem);

                inserted = newItem.MapTo<PDB, PDB_DTO>();

                FillJson(inserted);

                return inserted.Index > 0 ? inserted : null;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public List<PDB_DTO> NewMany(List<PDB> newItem, string dbName)
        {
            var fails = new List<PDB>();
            var success = new List<PDB_DTO>();
            PDB currentReq = null;
            try
            {
                foreach (var item in newItem)
                {
                    currentReq = item;
                    var inserted = New(item, dbName);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }
            }
            catch (Exception)
            {
                fails.Add(currentReq);
            }
            FillJson(success);
            return success;
        }
        public bool IsExists(PDB item, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<PDB>(dbName, Table_Requirement)
                                        .Find(x => x.Name.Equals(item.Name) && x.Index == item.Index);

                return result.Any();
            }
            catch (Exception)
            {
                return true;
            }
        }
        public PDB_DTO Update(PDB item, string dbName)
        {
            try
            {
                PDB_DTO inserted = null;
                var check = Db.Instance.GetCollection<PDB>(dbName, Table_Requirement)
                                        .Find(x => x.Name.Equals(item.Name));
                if (check.Any())
                    return inserted;

                Db.Instance.GetCollection<PDB>(dbName, Table_Requirement).ReplaceOne(x => x.Index == item.Index, item);
                inserted = item.MapTo<PDB, PDB_DTO>();
                FillJson(inserted);

                return inserted.Index > 0 ? inserted : null;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<PDB_DTO> UpdateMany(List<PDB> items, string dbName)
        {
            var fails = new List<PDB>();
            var success = new List<PDB_DTO>();
            PDB currentReq = null;
            try
            {
                foreach (var item in items)
                {
                    currentReq = item;
                    var inserted = Update(item, dbName);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }

            }
            catch (Exception)
            {
                fails.Add(currentReq);
            }
            FillJson(success);
            return success;
        }
        public PDB_DTO Get(int id, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<PDB>(dbName, Table_Requirement)
                                        .Find(x => x.Index == id);
                PDB_DTO inserted = null;

                if (result.Any())
                {
                    inserted = result.FirstOrDefault().MapTo<PDB, PDB_DTO>();
                    FillJson(inserted);
                }

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<PDB_DTO> Gets(PDB item, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<PDB_DTO>(dbName, Table_Requirement).ToList();

                var filtered = result.Where(x => (item.Name is null || item.Name.Equals(string.Empty) || x.Name.Equals(item.Name))
                                                && (item.Status is null || x.Status == item.Status));
                if (filtered != null && filtered.Any())
                    FillJson(filtered.ToList());

                return filtered.ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<PDB_DTO> DeleteMany(List<PDB> items, string dbName)
        {
            var fails = new List<PDB>();
            var success = new List<PDB_DTO>();
            var currentItem = items.FirstOrDefault();
            try
            {
                foreach (var item in items)
                {
                    currentItem = item;
                    var updated = Update(currentItem, dbName);
                    if (updated is null)
                        fails.Add(item);
                    else
                        success.Add(item.MapTo<PDB, PDB_DTO>());
                }
            }
            catch (Exception)
            {
                fails.Add(currentItem);
            }
            FillJson(success);
            return success;
        }
        private void FillJson(PDB_DTO inserted)
        {
            inserted.CreatedDateTimeStamp = inserted.CreatedDate.ToTimestamp();
            inserted.UpdatedDateTimeStamp = inserted.UpdatedDate.ToTimestamp();
            inserted.StatusType = GetStatus(inserted.Status);

        }
        private void FillJson(List<PDB_DTO> inserteds)
        {
            if (inserteds != null)
            {
                foreach (var inserted in inserteds)
                {
                    FillJson(inserted);
                }
            }
        }
        private StatusType GetStatus(bool? status)
        {
            switch (status)
            {
                case true:
                    return new StatusType("Active", "badge bg-success", true);
                case false:
                default:
                    return new StatusType("Inactive", "badge bg-danger");
            }
        }
    }
}