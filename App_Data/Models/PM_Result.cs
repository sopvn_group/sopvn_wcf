﻿using System.Runtime.Serialization;

public sealed partial class PMRes
{
    [DataContract]
    public class Result<T>
    {
        [DataMember]
        public T Data;
        [DataMember]
        public bool IsSuccess;
        [DataMember]
        public string Mes;
    }
}