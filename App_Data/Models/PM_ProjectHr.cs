﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;

/// <summary>
/// Summary description for PRO_HR
/// </summary>
public partial class PMRes
{
    [DataContract(Name = "ProjectHr")]
    public sealed class PRO_HR
    {
        [DataMember] public int Id { get; set; }
        [DataMember] public int ProjectId { get; set; }
        [DataMember] public int HrId { get; set; }
        [DataMember] public int PositionId { get; set; }
        [DataMember] public bool IsCreator { get; set; }
        [DataMember] public DateTime CreatedDate { get; set; }
        [DataMember] public DateTime UpdatedDate { get; set; }
        [DataMember] public bool Status { get; set; }
    }
    [DataContract(Name = "ProjectHrDTO")]
    public sealed class PRO_HR_DTO
    {
       [DataMember] public int Id { get; set; }
       [DataMember] public int ProjectId { get; set; }
       [DataMember] public string ProjectName { get; set; }
       [DataMember] public string ProjecAvatar { get; set; }
       [DataMember] public int HrId { get; set; }
       [DataMember] public string HrName { get; set; }
       [DataMember] public int PositionId { get; set; }
       [DataMember] public string PositionName { get; set; }
       [DataMember] public bool IsCreator { get; set; }
       [DataMember] public DateTime CreatedDate { get; set; }
       [DataMember] public DateTime UpdatedDate { get; set; }
       [DataMember] public double CreatedDateTimeStamp { get; set; }
       [DataMember] public double UpdatedDateTimeStamp { get; set; }
       [DataMember] public bool Status { get; set; }
       [DataMember] public StatusType StatusType { get; set; }
    }

    public sealed class PRO_HR_DAO
    {
        private static PRO_HR_DAO instance = null;
        public static PRO_HR_DAO Instance
        {
            get
            {
                instance = instance ?? new PRO_HR_DAO();
                return instance;
            }
        }
        private PRO_HR_DAO() { }
        public PRO_HR_DTO New(PRO_HR newItem)
        {
            try
            {
                PRO_HR_DTO inserted = null;

                if (IsExists(newItem))
                    return inserted;

                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Pro_Hr_Insert)
                                           .AddParam("@ProjectId", newItem.ProjectId)
                                           .AddParam("@HrId", newItem.HrId)
                                           .AddParam("@PositionId", newItem.PositionId)
                                           .AddParam("@IsCreator", newItem.IsCreator)
                                           .AddParam("@CreatedDate", DateTime.UtcNow)
                                           .Exec().ToList<PRO_HR_DTO>();

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public List<PRO_HR_DTO> NewMany(List<PRO_HR> newItem)
        {
            var fails = new List<PRO_HR>();
            var success = new List<PRO_HR_DTO>();
            PRO_HR currentPRO_HR = null;
            try
            {
                foreach (var item in newItem)
                {
                    currentPRO_HR = item;
                    var inserted = New(item);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }
            }
            catch (Exception)
            {
                fails.Add(currentPRO_HR);
            }
            FillJson(success);
            return success;
        }
        public bool IsExists(PRO_HR item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Pro_Hr_IsExists)
                                           .AddParam("@ProjectId", item.ProjectId)
                                           .AddParam("@HrId", item.HrId)
                                           .AddParam("@PositionId", item.PositionId)
                                           .Exec().ToList<PRO_HR>();

                return inserteds.Any();
            }
            catch (Exception)
            {
                return true;
            }
        }
        public PRO_HR_DTO Update(PRO_HR item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Pro_Hr_Update)
                                           .AddParam("@Id", item.Id)
                                           .AddParam("@ProjectId", item.ProjectId)
                                           .AddParam("@HrId", item.HrId)
                                           .AddParam("@PositionId", item.PositionId)
                                           .AddParam("@UpdatedDate", DateTime.UtcNow)
                                           .Exec().ToList<PRO_HR_DTO>();
                PRO_HR_DTO inserted = null;

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<PRO_HR_DTO> UpdateMany(List<PRO_HR> items)
        {
            var fails = new List<PRO_HR>();
            var success = new List<PRO_HR_DTO>();
            PRO_HR currentPRO_HR = null;
            try
            {
                foreach (var item in items)
                {
                    currentPRO_HR = item;
                    var inserted = Update(item);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }

            }
            catch (Exception)
            {
                fails.Add(currentPRO_HR);
            }
            FillJson(success);

            return success;
        }
        public PRO_HR_DTO Get(int id)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Pro_Hr_GetById)
                                           .AddParam("@Id", id)
                                           .Exec().ToList<PRO_HR_DTO>();
                PRO_HR_DTO inserted = null;

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<PRO_HR_DTO> Gets(PRO_HR_DTO item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Pro_Hr_Gets)
                                           .AddParam("@ProjectName", item.ProjectName)
                                           .AddParam("@HrName", item.HrName)
                                           .AddParam("@PositionName", item.PositionName)
                                           .AddParam("@Status", item.Status)
                                           .Exec().ToList<PRO_HR_DTO>();
                FillJson(inserteds);

                return inserteds;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool ChangeStatus(ref PRO_HR item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Pro_Hr_ChangeStatus)
                                           .AddParam("@Id", item.Id)
                                           .AddParam("@UpdatedDate", DateTime.UtcNow)
                                           .AddParam("@Status", item.Status)
                                           .Exec().ToList<PRO_HR>();
                var check = inserteds.Any();
                if (check)
                    item = inserteds.FirstOrDefault();

                return check;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public List<PRO_HR_DTO> DeleteMany(List<PRO_HR> items)
        {
            var fails = new List<PRO_HR>();
            var success = new List<PRO_HR_DTO>();
            var currentItem = items.FirstOrDefault();
            try
            {
                foreach (var item in items)
                {
                    currentItem = item;
                    if (!ChangeStatus(ref currentItem))
                        fails.Add(item);
                    else
                        success.Add(item.Map<PRO_HR, PRO_HR_DTO>());
                }
            }
            catch (Exception)
            {
                fails.Add(currentItem);
            }
            FillJson(success);
            return success;
        }
        private void FillJson(List<PRO_HR_DTO> inserteds)
        {
            if (inserteds != null)
            {
                foreach (var inserted in inserteds)
                {
                    inserted.CreatedDateTimeStamp = inserted.CreatedDate.ToTimestamp();
                    inserted.UpdatedDateTimeStamp = inserted.UpdatedDate.ToTimestamp();
                    inserted.StatusType = GetStatus(inserted.Status);
                }
            }
        }
        public StatusType GetStatus(bool status)
        {
            switch (status)
            {
                case true:
                    return new StatusType("Active", "badge bg-success", true);
                case false:
                default:
                    return new StatusType("Inactive", "badge bg-danger");
            }
        }
    }
}