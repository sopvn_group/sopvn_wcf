﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;

/// <summary>
/// Summary description for PM_HR
/// </summary>
public partial class PMRes
{
    [DataContract(Name = "Employee")]
    public sealed class HR
    {
        [DataMember] public int Id { get; set; }
        [DataMember] public string FirstName { get; set; }
        [DataMember] public string LastName { get; set; }
        [DataMember] public string FullName { get; set; }
        [DataMember] public string Email { get; set; }
        [DataMember] public string Password { get; set; }
        [DataMember] public string Contact { get; set; }
        [DataMember] public string Address { get; set; }
        [DataMember] public string Avatar { get; set; }
        [DataMember] public DateTime CreatedDate { get; set; }
        [DataMember] public DateTime UpdatedDate { get; set; }
        [DataMember] public string HistoryUpdate { get; set; }
        [DataMember] public string HistoryLogin { get; set; }
        [DataMember] public bool IsLogged { get; set; }
        [DataMember] public bool Status { get; set; }
    }
    [DataContract(Name = "EmployeeDTO")]
    public sealed class HR_DTO
    {
       [DataMember] public int Id { get; set; }
       [DataMember] public string FirstName { get; set; }
       [DataMember] public string LastName { get; set; }
       [DataMember] public string FullName { get; set; }
       [DataMember] public string Email { get; set; }
       [DataMember] public string Password { get; set; }
       [DataMember] public string Contact { get; set; }
       [DataMember] public string Address { get; set; }
       [DataMember] public string Avatar { get; set; }
       [DataMember] public DateTime CreatedDate { get; set; }
       [DataMember] public DateTime UpdatedDate { get; set; }
       [DataMember] public double CreatedDateTimeStamp { get; set; }
       [DataMember] public double UpdatedDateTimeStamp { get; set; }
       [DataMember] public string HistoryUpdate { get; set; }
       [DataMember] public string HistoryLogin { get; set; }
       [DataMember] public bool IsLogged { get; set; }
       [DataMember] public bool Status { get; set; }
       [DataMember] public StatusType StatusType { get; set; }
    }

    public sealed class HR_DAO
    {
        private static HR_DAO instance = null;
        public static HR_DAO Instance
        {
            get
            {
                instance = instance ?? new HR_DAO();
                return instance;
            }
        }
        private HR_DAO() { }
        public HR_DTO New(HR newItem)
        {
            try
            {
                HR_DTO inserted = null;

                if (IsExists(newItem.Email))
                    return inserted;

                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Hr_Insert)
                                           .AddParam("@FirstName", newItem.FirstName)
                                           .AddParam("@LastName", newItem.LastName)
                                           .AddParam("@FullName", newItem.FullName)
                                           .AddParam("@Email", newItem.Email)
                                           .AddParam("@Password", newItem.Password)
                                           .AddParam("@Contact", newItem.Contact)
                                           .AddParam("@CreatedDate", DateTime.UtcNow)
                                           .Exec().ToList<HR_DTO>();

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public List<HR_DTO> NewMany(List<HR> newItem)
        {
            var fails = new List<HR>();
            var success = new List<HR_DTO>();
            HR currentHR = null;
            try
            {
                foreach (var item in newItem)
                {
                    currentHR = item;
                    var inserted = New(item);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }
            }
            catch (Exception)
            {
                fails.Add(currentHR);
            }

            return success;
        }
        public bool IsExists(string email)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Hr_IsExists)
                                           .AddParam("@Email", email)
                                           .Exec().ToList<HR>();

                return inserteds.Any();
            }
            catch (Exception)
            {
                return true;
            }
        }
        public HR_DTO UpdateAvatar(HR item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Hr_UpdateAvatar)
                                           .AddParam("@Id", item.Id)
                                           .AddParam("@Avatar", item.Avatar)
                                           .AddParam("@CreatedDate", DateTime.UtcNow)
                                           .Exec().ToList<HR_DTO>();
                HR_DTO inserted = null;

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public HR_DTO UpdatePassword(HR item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Hr_UpdatePassword)
                                           .AddParam("@Id", item.Id)
                                           .AddParam("@Password", item.Password)
                                           .AddParam("@CreatedDate", DateTime.UtcNow)
                                           .Exec().ToList<HR_DTO>();
                HR_DTO inserted = null;

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public HR_DTO Update(HR item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Hr_Update)
                                           .AddParam("@FirstName", item.FirstName)
                                           .AddParam("@LastName", item.LastName)
                                           .AddParam("@FullName", item.FullName)
                                           .AddParam("@Email", item.Email)
                                           .AddParam("@Contact", item.Contact)
                                           .AddParam("@CreatedDate", DateTime.UtcNow)
                                           .AddParam("@Status", item.Status)
                                           .Exec().ToList<HR_DTO>();
                HR_DTO inserted = null;

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<HR_DTO> UpdateMany(List<HR> items)
        {
            var fails = new List<HR>();
            var success = new List<HR_DTO>();
            HR currentHR = null;
            try
            {
                foreach (var item in items)
                {
                    currentHR = item;
                    var inserted = Update(item);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }

            }
            catch (Exception)
            {
                fails.Add(currentHR);
            }

            return success;
        }
        public HR_DTO Get(int id)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Hr_GetById)
                                           .AddParam("@Id", id)
                                           .Exec().ToList<HR_DTO>();
                HR_DTO inserted = null;

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<HR_DTO> Gets(HR item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Hr_Gets)
                                           .AddParam("@FirstName", item.FirstName)
                                           .AddParam("@LastName", item.LastName)
                                           .AddParam("@FullName", item.FullName)
                                           .AddParam("@Email", item.Email)
                                           .AddParam("@Password", item.Password)
                                           .AddParam("@Contact", item.Contact)
                                           .AddParam("@Status", item.Status)
                                           .Exec().ToList<HR_DTO>();
                FillJson(inserteds);

                return inserteds;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool ChangeStatus(ref HR item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Hr_ChangeStatus)
                                           .AddParam("@Id", item.Id)
                                           .AddParam("@UpdatedDate", DateTime.UtcNow)
                                           .AddParam("@Status", item.Status)
                                           .Exec().ToList<HR>();
                var check = inserteds.Any();
                if (check)
                    item = inserteds.FirstOrDefault();

                return check;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public List<HR_DTO> DeleteMany(List<HR> items)
        {
            var fails = new List<HR>();
            var success = new List<HR_DTO>();
            var currentItem = items.FirstOrDefault();
            try
            {
                foreach (var item in items)
                {
                    currentItem = item;
                    if (!ChangeStatus(ref currentItem))
                        fails.Add(item);
                    else
                        success.Add(item.Map<HR, HR_DTO>());
                }
            }
            catch (Exception)
            {
                fails.Add(currentItem);
            }
            FillJson(success);
            return success;
        }
        private void FillJson(List<HR_DTO> inserteds)
        {
            if (inserteds != null)
            {
                foreach (var inserted in inserteds)
                {
                    inserted.CreatedDateTimeStamp = inserted.CreatedDate.ToTimestamp();
                    inserted.UpdatedDateTimeStamp = inserted.UpdatedDate.ToTimestamp();
                    inserted.StatusType = GetStatus(inserted.Status);
                }
            }
        }
        public StatusType GetStatus(bool status)
        {
            switch (status)
            {
                case true:
                    return new StatusType("Active", "badge bg-success", true);
                case false:
                default:
                    return new StatusType("Inactive", "badge bg-danger");
            }
        }
    }
}