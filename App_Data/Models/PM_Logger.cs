﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

/// <summary>
/// Summary description for PM_Logger
/// </summary>
public partial class PMRes
{
    public sealed class Logger
    {
        public string Id { get; set; }
        public string Content { get; set; }
        public dynamic Data { get; set; }
        public bool IsError { get; set; }
        public DateTime CreatedDate { get; set; }

        public Logger() { }

    }

    public sealed class LoggerDTO
    {
        public string Id { get; set; }
        public string Content { get; set; }
        public dynamic Data { get; set; }
        public string DataJson { get; set; }
        public bool IsError { get; set; }
        public DateTime CreatedDate { get; set; }

        public LoggerDTO() { }
    }

    public sealed class Logger_DAO
    {
        private static Logger_DAO instance = null;
        public static Logger_DAO Instance
        {
            get
            {
                instance = instance ?? new Logger_DAO();
                return instance;
            }
        }
        public void Log(string content)
        {
            Db.Instance.Store(PMRes.Instance.Sp_Log_Insert)
                       .AddParam("@Content", content)
                       .AddParam("@DataJson", "")
                       .AddParam("@IsError", 0)
                       .AddParam("@CreatedDate", DateTime.UtcNow)
                       .Exec();

        }
        public void Log(string content, object data)
        {
            Db.Instance.Store(PMRes.Instance.Sp_Log_Insert)
                       .AddParam("@Content", content)
                       .AddParam("@DataJson", JsonConvert.SerializeObject(data))
                       .AddParam("@IsError", 0)
                       .AddParam("@CreatedDate", DateTime.UtcNow)
                       .Exec();

        }
        public void Error(string content)
        {
            Db.Instance.Store(PMRes.Instance.Sp_Log_Insert)
                       .AddParam("@Content", content)
                       .AddParam("@DataJson", "")
                       .AddParam("@IsError", 1)
                       .AddParam("@CreatedDate", DateTime.UtcNow)
                       .Exec();

        }
        public void Error(string content, object data)
        {
            Db.Instance.Store(PMRes.Instance.Sp_Log_Insert)
                       .AddParam("@Content", content)
                       .AddParam("@DataJson", JsonConvert.SerializeObject(data))
                       .AddParam("@IsError", 1)
                       .AddParam("@CreatedDate", DateTime.UtcNow)
                       .Exec();

        }
        public List<LoggerDTO> Gets()
        {
            try
            {

                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}