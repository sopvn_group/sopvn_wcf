﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;

public sealed partial class PMRes
{
    [DataContract(Name = "UserStory")]
    public sealed class US
    {
        [DataMember] public ObjectId _id { get; set; }
        [DataMember] public int Index { get; set; }
        [DataMember] public int ReqId { get; set; }
        [DataMember] public string Title { get; set; }
        [DataMember] public string Description { get; set; }
        [DataMember] public DateTime CreatedDate { get; set; }
        [DataMember] public DateTime UpdatedDate { get; set; }
        [DataMember] public bool? Status { get; set; }
        [DataMember] public List<int> SprintDetailIds { get; set; }
    }
    [DataContract(Name = "UserStoryDTO")]
    public sealed class US_DTO
    {
        [DataMember] public ObjectId _id { get; set; }
        [DataMember] public int Index { get; set; }
        [DataMember] public int ReqId { get; set; }
        [DataMember] public string Title { get; set; }
        [DataMember] public string Description { get; set; }
        [DataMember] public DateTime CreatedDate { get; set; }
        [DataMember] public DateTime UpdatedDate { get; set; }
        [DataMember] public double CreatedDateTimeStamp { get; set; }
        [DataMember] public double UpdatedDateTimeStamp { get; set; }
        [DataMember] public bool Status { get; set; }
        [DataMember] public StatusType StatusType { get; set; }
        [DataMember] public List<int> SprintDetailIds { get; set; }
    }

    public sealed class US_DAO
    {
        private static US_DAO instance = null;
        public static US_DAO Instance
        {
            get
            {
                instance = instance ?? new US_DAO();
                return instance;
            }
        }
        private US_DAO() { }

        public US_DTO New(US newItem, string dbName)
        {
            try
            {
                US_DTO inserted = null;

                if (IsExists(newItem, dbName))
                    return inserted;

                Db.Instance.GetCollection<US>(dbName, Table_UserStory).InsertOneAsync(newItem);

                inserted = newItem.Map<US, US_DTO>();

                FillJson(inserted);

                return inserted.Index > 0 ? inserted : null;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public List<US_DTO> NewMany(List<US> newItem, string dbName)
        {
            var fails = new List<US>();
            var success = new List<US_DTO>();
            US currentUS = null;
            try
            {
                foreach (var item in newItem)
                {
                    currentUS = item;
                    var inserted = New(item, dbName);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }
            }
            catch (Exception)
            {
                fails.Add(currentUS);
            }
            FillJson(success);
            return success;
        }
        public bool IsExists(US item, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<US>(dbName, Table_UserStory)
                                        .Find(x => x.ReqId == item.ReqId && x.Index == item.Index);

                return result.Any();
            }
            catch (Exception)
            {
                return true;
            }
        }
        public US_DTO Update(US item, string dbName)
        {
            try
            {
                US_DTO inserted = null;
                var check = Db.Instance.GetCollection<US>(dbName, Table_UserStory)
                                        .Find(x => x.ReqId == item.ReqId && x.Index == item.Index);
                if (check.Any())
                    return inserted;

                Db.Instance.GetCollection<US>(dbName, Table_UserStory).ReplaceOne(x => x.Index == item.Index, item);
                inserted = item.Map<US, US_DTO>();
                FillJson(inserted);

                return inserted.Index > 0 ? inserted : null;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<US_DTO> UpdateMany(List<US> items, string dbName)
        {
            var fails = new List<US>();
            var success = new List<US_DTO>();
            US currentUS = null;
            try
            {
                foreach (var item in items)
                {
                    currentUS = item;
                    var inserted = Update(item, dbName);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }

            }
            catch (Exception)
            {
                fails.Add(currentUS);
            }
            FillJson(success);
            return success;
        }
        public US_DTO Get(int id, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<US>(dbName, Table_UserStory)
                                        .Find(x => x.Index == id);
                US_DTO inserted = null;

                if (result.Any())
                {
                    inserted = result.FirstOrDefault().Map<US, US_DTO>();
                    FillJson(inserted);
                }

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<US_DTO> Gets(US item, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<US_DTO>(dbName, Table_UserStory).ToList();

                var filtered = result.Where(x => (item.Description is null || item.Description.Equals(string.Empty) || x.Description.Contains(item.Description))
                                                && (item.Title is null || item.Title.Equals(string.Empty) || x.Title.Contains(item.Title))
                                                && (item.CreatedDate == null || x.CreatedDate == item.CreatedDate)
                                                && (item.UpdatedDate == null || x.UpdatedDate == item.UpdatedDate)
                                                && (item.Status is null || x.Status == item.Status));
                if (filtered != null && filtered.Any())
                    FillJson(filtered.ToList());

                return filtered.ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<US_DTO> DeleteMany(List<US> items, string dbName)
        {
            var fails = new List<US>();
            var success = new List<US_DTO>();
            var currentItem = items.FirstOrDefault();
            try
            {
                foreach (var item in items)
                {
                    currentItem = item;
                    var updated = Update(currentItem, dbName);
                    if (updated is null)
                        fails.Add(item);
                    else
                        success.Add(item.Map<US, US_DTO>());
                }
            }
            catch (Exception)
            {
                fails.Add(currentItem);
            }
            FillJson(success);
            return success;
        }
        private void FillJson(US_DTO inserted)
        {
            inserted.CreatedDateTimeStamp = inserted.CreatedDate.ToTimestamp();
            inserted.UpdatedDateTimeStamp = inserted.UpdatedDate.ToTimestamp();
            inserted.StatusType = GetStatus(inserted.Status);

        }
        private void FillJson(List<US_DTO> inserteds)
        {
            if (inserteds != null)
            {
                foreach (var inserted in inserteds)
                {
                    FillJson(inserted);
                }
            }
        }
        private StatusType GetStatus(bool? status)
        {
            switch (status)
            {
                case true:
                    return new StatusType("Active", "badge bg-success", true);
                case false:
                default:
                    return new StatusType("Inactive", "badge bg-danger");
            }
        }
    }
}