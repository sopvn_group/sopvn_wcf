﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;

/// <summary>
/// Summary description for PRO_PER
/// </summary>
public partial class PMRes
{
    [DataContract(Name = "ProjectPermision")]
    public sealed class PRO_PER
    {
        [DataMember] public int Id { get; set; }
        [DataMember] public int ProjectId { get; set; }
        [DataMember] public int HrId { get; set; }
        [DataMember] public int PermisionId { get; set; }
        [DataMember] public bool IsCreator { get; set; }
        [DataMember] public DateTime CreatedDate { get; set; }
        [DataMember] public DateTime UpdatedDate { get; set; }
        [DataMember] public bool Status { get; set; }
    }
    [DataContract(Name = "ProjectPermisionDTO")]
    public sealed class PRO_PER_DTO
    {
        [DataMember] public int Id { get; set; }
        [DataMember] public int ProjectId { get; set; }
        [DataMember] public string ProjectName { get; set; }
        [DataMember] public string ProjecAvatar { get; set; }
        [DataMember] public int HrId { get; set; }
        [DataMember] public string HrName { get; set; }
        [DataMember] public int PermisionId { get; set; }
        [DataMember] public string PermisionName { get; set; }
        [DataMember] public bool IsCreator { get; set; }
        [DataMember] public DateTime CreatedDate { get; set; }
        [DataMember] public DateTime UpdatedDate { get; set; }
        [DataMember] public double CreatedDateTimeStamp { get; set; }
        [DataMember] public double UpdatedDateTimeStamp { get; set; }
        [DataMember] public bool Status { get; set; }
        [DataMember] public StatusType StatusType { get; set; }
    }

    public sealed class PRO_PER_DAO
    {
        private static PRO_PER_DAO instance = null;
        public static PRO_PER_DAO Instance
        {
            get
            {
                instance = instance ?? new PRO_PER_DAO();
                return instance;
            }
        }
        private PRO_PER_DAO() { }
        public PRO_PER_DTO New(PRO_PER newItem)
        {
            try
            {
                PRO_PER_DTO inserted = null;

                if (IsExists(newItem))
                    return inserted;

                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Pro_Per_Insert)
                                           .AddParam("@ProjectId", newItem.ProjectId)
                                           .AddParam("@HrId", newItem.HrId)
                                           .AddParam("@PermisionId", newItem.PermisionId)
                                           .AddParam("@CreatedDate", DateTime.UtcNow)
                                           .Exec().ToList<PRO_PER_DTO>();

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public List<PRO_PER_DTO> NewMany(List<PRO_PER> newItem)
        {
            var fails = new List<PRO_PER>();
            var success = new List<PRO_PER_DTO>();
            PRO_PER currentPRO_PER = null;
            try
            {
                foreach (var item in newItem)
                {
                    currentPRO_PER = item;
                    var inserted = New(item);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }
            }
            catch (Exception)
            {
                fails.Add(currentPRO_PER);
            }
            FillJson(success);
            return success;
        }
        public bool IsExists(PRO_PER item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Pro_Per_IsExists)
                                           .AddParam("@ProjectId", item.ProjectId)
                                           .AddParam("@HrId", item.HrId)
                                           .AddParam("@PermisionId", item.PermisionId)
                                           .Exec().ToList<PRO_PER>();

                return inserteds.Any();
            }
            catch (Exception)
            {
                return true;
            }
        }
        public PRO_PER_DTO Update(PRO_PER item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Pro_Per_Update)
                                           .AddParam("@ProjectId", item.ProjectId)
                                           .AddParam("@HrId", item.HrId)
                                           .AddParam("@PermisionId", item.PermisionId)
                                           .AddParam("@UpdatedDate", DateTime.UtcNow)
                                           .Exec().ToList<PRO_PER_DTO>();
                PRO_PER_DTO inserted = null;

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<PRO_PER_DTO> UpdateMany(List<PRO_PER> items)
        {
            var fails = new List<PRO_PER>();
            var success = new List<PRO_PER_DTO>();
            PRO_PER currentPRO_PER = null;
            try
            {
                foreach (var item in items)
                {
                    currentPRO_PER = item;
                    var inserted = Update(item);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }

            }
            catch (Exception)
            {
                fails.Add(currentPRO_PER);
            }
            FillJson(success);
            return success;
        }
        public PRO_PER_DTO Get(int id)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Pro_Per_GetById)
                                           .AddParam("@Id", id)
                                           .Exec().ToList<PRO_PER_DTO>();
                PRO_PER_DTO inserted = null;

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<PRO_PER_DTO> Gets(PRO_PER_DTO item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Pro_Per_Gets)
                                           .AddParam("@ProjectName", item.ProjectName)
                                           .AddParam("@HrName", item.HrName)
                                           .AddParam("@PermisionName", item.PermisionName)
                                           .AddParam("@Status", item.Status)
                                           .Exec().ToList<PRO_PER_DTO>();
                FillJson(inserteds);

                return inserteds;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool ChangeStatus(ref PRO_PER item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Pro_Per_ChangeStatus)
                                           .AddParam("@Id", item.Id)
                                           .AddParam("@UpdatedDate", DateTime.UtcNow)
                                           .AddParam("@Status", item.Status)
                                           .Exec().ToList<PRO_PER>();
                var check = inserteds.Any();
                if (check)
                    item = inserteds.FirstOrDefault();

                return check;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public List<PRO_PER_DTO> DeleteMany(List<PRO_PER> items)
        {
            var fails = new List<PRO_PER>();
            var success = new List<PRO_PER_DTO>();
            var currentItem = items.FirstOrDefault();
            try
            {
                foreach (var item in items)
                {
                    currentItem = item;
                    if (!ChangeStatus(ref currentItem))
                        fails.Add(item);
                    else
                        success.Add(item.Map<PRO_PER, PRO_PER_DTO>());
                }
            }
            catch (Exception)
            {
                fails.Add(currentItem);
            }
            FillJson(success);
            return success;
        }
        private void FillJson(List<PRO_PER_DTO> inserteds)
        {
            if (inserteds != null)
            {
                foreach (var inserted in inserteds)
                {
                    inserted.CreatedDateTimeStamp = inserted.CreatedDate.ToTimestamp();
                    inserted.UpdatedDateTimeStamp = inserted.UpdatedDate.ToTimestamp();
                    inserted.StatusType = GetStatus(inserted.Status);
                }
            }
        }
        private StatusType GetStatus(bool status)
        {
            switch (status)
            {
                case true:
                    return new StatusType("Active", "badge bg-success", true);
                case false:
                default:
                    return new StatusType("Inactive", "badge bg-danger");
            }
        }
    }
}