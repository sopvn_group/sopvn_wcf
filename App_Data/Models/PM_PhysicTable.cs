﻿using MongoDB.Bson;
using System.Runtime.Serialization;
using System;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;

public sealed partial class PMRes
{
    [DataContract(Name = "PhysicTable")]
    public sealed class PT
    {
        [DataMember] public ObjectId _id { get; set; }
        [DataMember] public int Index { get; set; }
        [DataMember] public int TableId { get; set; }
        [DataMember] public string FieldName { get; set; }
        [DataMember] public string DataType { get; set; }
        [DataMember] public bool IsRequired { get; set; }
        [DataMember] public string Key { get; set; }
        [DataMember] public string Description { get; set; }
        [DataMember] public DateTime? CreatedDate { get; set; }
        [DataMember] public DateTime? UpdatedDate { get; set; }
        [DataMember] public bool? Status { get; set; }
    }
    [DataContract(Name = "PhysicTableDTO")]
    public sealed class PT_DTO
    {
        [DataMember] public ObjectId _id { get; set; }
        [DataMember] public int Index { get; set; }
        [DataMember] public int TableId { get; set; }
        [DataMember] public string FieldName { get; set; }
        [DataMember] public string DataType { get; set; }
        [DataMember] public bool IsRequired { get; set; }
        [DataMember] public string Key { get; set; }
        [DataMember] public string Description { get; set; }
        [DataMember] public DateTime CreatedDate { get; set; }
        [DataMember] public DateTime UpdatedDate { get; set; }
        [DataMember] public double CreatedDateTimeStamp { get; set; }
        [DataMember] public double UpdatedDateTimeStamp { get; set; }
        [DataMember] public bool? Status { get; set; }
        [DataMember] public StatusType StatusType { get; set; }
    }

    public sealed class PT_DAO
    {
        private static PT_DAO instance = null;
        public static PT_DAO Instance
        {
            get
            {
                instance = instance ?? new PT_DAO();
                return instance;
            }
        }
        private PT_DAO() { }
        public PT_DTO New(PT newItem, string dbName)
        {
            try
            {
                PT_DTO inserted = null;

                if (IsExists(newItem, dbName))
                    return inserted;

                Db.Instance.GetCollection<PT>(dbName, Table_Requirement).InsertOneAsync(newItem);

                inserted = newItem.Map<PT, PT_DTO>();

                FillJson(inserted);

                return inserted.Index > 0 ? inserted : null;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public List<PT_DTO> NewMany(List<PT> newItem, string dbName)
        {
            var fails = new List<PT>();
            var success = new List<PT_DTO>();
            PT currentReq = null;
            try
            {
                foreach (var item in newItem)
                {
                    currentReq = item;
                    var inserted = New(item, dbName);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }
            }
            catch (Exception)
            {
                fails.Add(currentReq);
            }
            FillJson(success);
            return success;
        }
        public bool IsExists(PT item, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<PT>(dbName, Table_Requirement)
                                        .Find(x => x.FieldName.Equals(item.FieldName) && x.Index == item.Index);

                return result.Any();
            }
            catch (Exception)
            {
                return true;
            }
        }
        public PT_DTO Update(PT item, string dbName)
        {
            try
            {
                PT_DTO inserted = null;
                var check = Db.Instance.GetCollection<PT>(dbName, Table_Requirement)
                                        .Find(x => x.FieldName.Equals(item.FieldName));
                if (check.Any())
                    return inserted;

                Db.Instance.GetCollection<PT>(dbName, Table_Requirement).ReplaceOne(x => x.Index == item.Index, item);
                inserted = item.Map<PT, PT_DTO>();
                FillJson(inserted);

                return inserted.Index > 0 ? inserted : null;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<PT_DTO> UpdateMany(List<PT> items, string dbName)
        {
            var fails = new List<PT>();
            var success = new List<PT_DTO>();
            PT currentReq = null;
            try
            {
                foreach (var item in items)
                {
                    currentReq = item;
                    var inserted = Update(item, dbName);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }

            }
            catch (Exception)
            {
                fails.Add(currentReq);
            }
            FillJson(success);
            return success;
        }
        public PT_DTO Get(int id, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<PT>(dbName, Table_Requirement)
                                        .Find(x => x.Index == id);
                PT_DTO inserted = null;

                if (result.Any())
                {
                    inserted = result.FirstOrDefault().Map<PT, PT_DTO>();
                    FillJson(inserted);
                }

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<PT_DTO> Gets(PT item, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<PT_DTO>(dbName, Table_Requirement).ToList();

                var filtered = result.Where(x => (item.FieldName is null || item.FieldName.Equals(string.Empty) || x.FieldName.Equals(item.FieldName))
                                                && (item.Status is null || x.Status == item.Status));
                if (filtered != null && filtered.Any())
                    FillJson(filtered.ToList());

                return filtered.ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<PT_DTO> DeleteMany(List<PT> items, string dbName)
        {
            var fails = new List<PT>();
            var success = new List<PT_DTO>();
            var currentItem = items.FirstOrDefault();
            try
            {
                foreach (var item in items)
                {
                    currentItem = item;
                    var updated = Update(currentItem, dbName);
                    if (updated is null)
                        fails.Add(item);
                    else
                        success.Add(item.Map<PT, PT_DTO>());
                }
            }
            catch (Exception)
            {
                fails.Add(currentItem);
            }
            FillJson(success);
            return success;
        }
        private void FillJson(PT_DTO inserted)
        {
            inserted.CreatedDateTimeStamp = inserted.CreatedDate.ToTimestamp();
            inserted.UpdatedDateTimeStamp = inserted.UpdatedDate.ToTimestamp();
            inserted.StatusType = GetStatus(inserted.Status);

        }
        private void FillJson(List<PT_DTO> inserteds)
        {
            if (inserteds != null)
            {
                foreach (var inserted in inserteds)
                {
                    FillJson(inserted);
                }
            }
        }
        private StatusType GetStatus(bool? status)
        {
            switch (status)
            {
                case true:
                    return new StatusType("Active", "badge bg-success", true);
                case false:
                default:
                    return new StatusType("Inactive", "badge bg-danger");
            }
        }
    }
}