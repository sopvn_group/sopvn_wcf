﻿/// <summary>
/// Summary description for Pm_StoreName
/// </summary>
public partial class PMRes
{
    public string Sp_Logs_Gets = "Sp_Logs_Gets";
    public string Sp_Log_Insert = "Sp_Log_Insert";
    #region Position
    public string Sp_Position_Insert = "Sp_Position_Insert";
    public string Sp_Position_Update = "Sp_Position_Update";
    public string Sp_Position_GetById = "Sp_Position_GetById";
    public string Sp_Position_GetAllPosition = "Sp_Position_GetAllPosition";
    public string Sp_Position_ChangeStatus = "Sp_Position_ChangeStatus";
    public string Sp_Position_IsExists = "Sp_Position_IsExists";
    public string Sp_Position_Gets = "Sp_Position_Gets";
    #endregion

    #region Permision
    public string Sp_Permision_Insert = "Sp_Permision_Insert";
    public string Sp_Permision_Update = "Sp_Permision_Update";
    public string Sp_Permision_GetById = "Sp_Permision_GetById";
    public string Sp_Permision_ChangeStatus = "Sp_Permision_ChangeStatus";
    public string Sp_Permision_IsExists = "Sp_Permision_IsExists";
    public string Sp_Permision_Gets = "Sp_Permision_Gets";
    #endregion

    #region Hr
    public string Sp_Hr_Insert = "Sp_Hr_Insert";
    public string Sp_Hr_Update = "Sp_Hr_Update";
    public string Sp_Hr_UpdateAvatar = "Sp_Hr_UpdateAvatar";
    public string Sp_Hr_UpdatePassword = "Sp_Hr_Update";
    public string Sp_Hr_GetById = "Sp_Hr_GetById";
    public string Sp_Hr_ChangeStatus = "Sp_Hr_ChangeStatus";
    public string Sp_Hr_IsExists = "Sp_Hr_IsExists";
    public string Sp_Hr_Gets = "Sp_Hr_Gets";
    #endregion

    #region Project
    public string Sp_Project_Insert = "Sp_Project_Insert";
    public string Sp_Project_Update = "Sp_Project_Update";
    public string Sp_Project_UpdateAvatar = "Sp_Project_UpdateAvatar";
    public string Sp_Project_GetById = "Sp_Project_GetById";
    public string Sp_Project_ChangeStatus = "Sp_Project_ChangeStatus";
    public string Sp_Project_IsExists = "Sp_Project_IsExists";
    public string Sp_Project_Gets = "Sp_Project_Gets";
    #endregion

    #region Project Hr
    public string Sp_Pro_Hr_Insert = "Sp_Pro_Hr_Insert";
    public string Sp_Pro_Hr_Update = "Sp_Pro_Hr_Update";
    public string Sp_Pro_Hr_UpdateAvatar = "Sp_Pro_Hr_UpdateAvatar";
    public string Sp_Pro_Hr_GetById = "Sp_Pro_Hr_GetById";
    public string Sp_Pro_Hr_ChangeStatus = "Sp_Pro_Hr_ChangeStatus";
    public string Sp_Pro_Hr_IsExists = "Sp_Pro_Hr_IsExists";
    public string Sp_Pro_Hr_Gets = "Sp_Pro_Hr_Gets";
    #endregion

    #region Project Permision
    public string Sp_Pro_Per_Insert = "Sp_Pro_Per_Insert";
    public string Sp_Pro_Per_Update = "Sp_Pro_Per_Update";
    public string Sp_Pro_Per_UpdateAvatar = "Sp_Pro_Per_UpdateAvatar";
    public string Sp_Pro_Per_GetById = "Sp_Pro_Per_GetById";
    public string Sp_Pro_Per_ChangeStatus = "Sp_Pro_Per_ChangeStatus";
    public string Sp_Pro_Per_IsExists = "Sp_Pro_Per_IsExists";
    public string Sp_Pro_Per_Gets = "Sp_Pro_Per_Gets";
    #endregion

    #region Requirement
    public string Sp_Req_Insert = "Sp_Req_Insert";
    public string Sp_Req_Update = "Sp_Req_Update";
    public string Sp_Req_UpdateAvatar = "Sp_Req_UpdateAvatar";
    public string Sp_Req_GetById = "Sp_Req_GetById";
    public string Sp_Req_ChangeStatus = "Sp_Req_ChangeStatus";
    public string Sp_Req_IsExists = "Sp_Req_IsExists";
    public string Sp_Req_Gets = "Sp_Req_Gets";
    #endregion
}