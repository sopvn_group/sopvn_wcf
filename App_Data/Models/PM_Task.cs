﻿using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

public sealed partial class PMRes
{
    [DataContract(Name = "Task")]
    public sealed class TASK
    {
        [DataMember] ObjectId _id { get; set; }
        [DataMember] public int Index { get; set; }
        [DataMember] public int SprintId { get; set; }
        [DataMember] public int UserStoryId { get; set; }
        [DataMember] public int Point { get; set; }
        [DataMember] public int Progress { get; set; }
        [DataMember] public int DevId { get; set; }
        [DataMember] public int TestId { get; set; }
        [DataMember] public string TestCase { get; set; }
        [DataMember] public DateTime DueDate { get; set; }
        [DataMember] public DateTime CreatedDate { get; set; }
        [DataMember] public DateTime UpdatedDate { get; set; }
        [DataMember] public int Status { get; set; }
    }

    [DataContract(Name = "TaskDTO")]
    public sealed class TASK_DTO
    {
        [DataMember] public ObjectId _id { get; set; }
        [DataMember] public int Index { get; set; }
        [DataMember] public int SprintId { get; set; }
        [DataMember] public string SprintTitle { get; set; }
        [DataMember] public int UserStoryId { get; set; }
        [DataMember] public string UserStoryTitle { get; set; }
        [DataMember] public int Point { get; set; }
        [DataMember] public int Progress { get; set; }
        [DataMember] public int DevId { get; set; }
        [DataMember] public string DevName { get; set; }
        [DataMember] public int TestId { get; set; }
        [DataMember] public string TestName { get; set; }
        [DataMember] public string TestCase { get; set; }
        [DataMember] public DateTime DueDate { get; set; }
        [DataMember] public DateTime CreatedDate { get; set; }
        [DataMember] public DateTime UpdatedDate { get; set; }
        [DataMember] public double DueDateTimeStamp { get; set; }
        [DataMember] public double CreatedDateTimeStamp { get; set; }
        [DataMember] public double UpdatedDateTimeStamp { get; set; }
        [DataMember] public int Status { get; set; }
        [DataMember] public StatusType StatusType { get; set; }
    }

    public sealed class TASK_DAO
    {
        private static TASK_DAO instance = null;
        public static TASK_DAO Instance
        {
            get
            {
                instance = instance ?? new TASK_DAO();
                return instance;
            }
        }
        private TASK_DAO() { }
        public TASK_DTO New(TASK newItem, string dbName)
        {
            try
            {
                TASK_DTO inserted = null;

                if (IsExists(newItem, dbName))
                    return inserted;

                Db.Instance.GetCollection<TASK>(dbName, Table_Task).InsertOneAsync(newItem);

                inserted = newItem.Map<TASK, TASK_DTO>();

                FillJson(inserted);

                return inserted.Index > 0 ? inserted : null;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public List<TASK_DTO> NewMany(List<TASK> newItem, string dbName)
        {
            var fails = new List<TASK>();
            var success = new List<TASK_DTO>();
            TASK currentTASK = null;
            try
            {
                foreach (var item in newItem)
                {
                    currentTASK = item;
                    var inserted = New(item, dbName);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }
            }
            catch (Exception)
            {
                fails.Add(currentTASK);
            }
            FillJson(success);
            return success;
        }
        public bool IsExists(TASK item, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<TASK>(dbName, Table_Task)
                                        .Find(x => x.SprintId == item.SprintId
                                                    && x.DevId == item.DevId
                                                    && x.Index == item.Index);

                return result.Any();
            }
            catch (Exception)
            {
                return true;
            }
        }
        public TASK_DTO Update(TASK item, string dbName)
        {
            try
            {
                TASK_DTO inserted = null;
                var check = Db.Instance.GetCollection<TASK>(dbName, Table_Task)
                                        .Find(x => x.SprintId == item.SprintId
                                                    && x.DevId == item.DevId
                                                    && x.Index == item.Index);
                if (check.Any())
                    return inserted;

                Db.Instance.GetCollection<TASK>(dbName, Table_Task).ReplaceOne(x => x.Index == item.Index, item);
                inserted = item.Map<TASK, TASK_DTO>();
                FillJson(inserted);

                return inserted.Index > 0 ? inserted : null;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<TASK_DTO> UpdateMany(List<TASK> items, string dbName)
        {
            var fails = new List<TASK>();
            var success = new List<TASK_DTO>();
            TASK currentTASK = null;
            try
            {
                foreach (var item in items)
                {
                    currentTASK = item;
                    var inserted = Update(item, dbName);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }

            }
            catch (Exception)
            {
                fails.Add(currentTASK);
            }
            FillJson(success);
            return success;
        }
        public TASK_DTO Get(int id, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<TASK>(dbName, Table_Task)
                                        .Find(x => x.Index == id);
                TASK_DTO inserted = null;

                if (result.Any())
                {
                    inserted = result.FirstOrDefault().Map<TASK, TASK_DTO>();
                    FillJson(inserted);
                }

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<TASK_DTO> Gets(TASK_DTO item, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<TASK_DTO>(dbName, Table_Task).ToList();

                var filtered = result.Where(x => (item.SprintTitle is null || item.SprintTitle.Equals(string.Empty) || x.SprintTitle.Equals(item.SprintTitle))
                                                && (item.UserStoryTitle is null || item.UserStoryTitle.Equals(string.Empty) || x.UserStoryTitle.Equals(item.UserStoryTitle))
                                                && (item.DevName is null || item.DevName.Equals(string.Empty) || x.DevName.Equals(item.DevName))
                                                && (item.TestName is null || item.TestName.Equals(string.Empty) || x.TestName.Equals(item.TestName))
                                                && (item.Progress == 0 || x.Progress == item.Progress)
                                                && (item.CreatedDate == null || x.CreatedDate == item.CreatedDate)
                                                && (item.UpdatedDate == null || x.UpdatedDate == item.UpdatedDate)
                                                && (item.Status == 0 || x.Status == item.Status));
                if (filtered != null && filtered.Any())
                    FillJson(filtered.ToList());

                return filtered.ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<TASK_DTO> DeleteMany(List<TASK> items, string dbName)
        {
            var fails = new List<TASK>();
            var success = new List<TASK_DTO>();
            var currentItem = items.FirstOrDefault();
            try
            {
                foreach (var item in items)
                {
                    currentItem = item;
                    var updated = Update(currentItem, dbName);
                    if (updated is null)
                        fails.Add(item);
                    else
                        success.Add(item.Map<TASK, TASK_DTO>());
                }
            }
            catch (Exception)
            {
                fails.Add(currentItem);
            }
            FillJson(success);
            return success;
        }
        private void FillJson(TASK_DTO inserted)
        {
            inserted.CreatedDateTimeStamp = inserted.CreatedDate.ToTimestamp();
            inserted.UpdatedDateTimeStamp = inserted.UpdatedDate.ToTimestamp();
            inserted.StatusType = GetStatus(inserted.Status);

        }
        private void FillJson(List<TASK_DTO> inserteds)
        {
            if (inserteds != null)
            {
                foreach (var inserted in inserteds)
                {
                    FillJson(inserted);
                }
            }
        }
        private StatusType GetStatus(int? status)
        {
            switch (status)
            {
                case 1:
                    return new StatusType("In Progress", "badge bg-success", true);
                case 2:
                    return new StatusType("Pending", "badge bg-warning", true);
                case 3:
                    return new StatusType("Complete", "badge bg-success", true);
                default:
                    return new StatusType("Cancle", "badge bg-danger");
            }
        }
    }
}