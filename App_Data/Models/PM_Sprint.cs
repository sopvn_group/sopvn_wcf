﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

public sealed partial class PMRes
{
    [DataContract(Name = "Sprint")]
    public sealed class SPRINT
    {
       [DataMember] public ObjectId _id { get; set; }
       [DataMember] public int Index { get; set; }
       [DataMember] public int Point { get; set; }
       [DataMember] public int Progress { get; set; }
       [DataMember] public string Title { get; set; }
       [DataMember] public DateTime CreatedDate { get; set; }
       [DataMember] public DateTime UpdatedDate { get; set; }
       [DataMember] public int Status { get; set; }
       [DataMember] public List<int> SprintDetailIds { get; set; }
       [DataMember] public List<int> SprintTestIds { get; set; }
    }
    [DataContract(Name = "SprintDTO")]
    public sealed class SPRINT_DTO
    {
        [DataMember] public ObjectId _id { get; set; }
        [DataMember] public int Index { get; set; }
        [DataMember] public int Point { get; set; }
        [DataMember] public int Progress { get; set; }
        [DataMember] public string Title { get; set; }
        [DataMember] public DateTime CreatedDate { get; set; }
        [DataMember] public DateTime UpdatedDate { get; set; }
        [DataMember] public double CreatedDateTimeStamp { get; set; }
        [DataMember] public double UpdatedDateTimeStamp { get; set; }
        [DataMember] public int Status { get; set; }
        [DataMember] public StatusType StatusType { get; set; }
        [DataMember] public List<int> SprintDetailIds { get; set; }
        [DataMember] public List<int> SprintTestIds { get; set; }
    }
    public sealed class SPRINT_DAO
    {
        private static SPRINT_DAO instance = null;
        public static SPRINT_DAO Instance
        {
            get
            {
                instance = instance ?? new SPRINT_DAO();
                return instance;
            }
        }
        private SPRINT_DAO() { }
        public SPRINT_DTO New(SPRINT newItem, string dbName)
        {
            try
            {
                SPRINT_DTO inserted = null;

                if (IsExists(newItem, dbName))
                    return inserted;

                Db.Instance.GetCollection<SPRINT>(dbName, Table_Sprint).InsertOneAsync(newItem);

                inserted = newItem.Map<SPRINT, SPRINT_DTO>();

                FillJson(inserted);

                return inserted.Index > 0 ? inserted : null;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public List<SPRINT_DTO> NewMany(List<SPRINT> newItem, string dbName)
        {
            var fails = new List<SPRINT>();
            var success = new List<SPRINT_DTO>();
            SPRINT currentSPRINT = null;
            try
            {
                foreach (var item in newItem)
                {
                    currentSPRINT = item;
                    var inserted = New(item, dbName);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }
            }
            catch (Exception)
            {
                fails.Add(currentSPRINT);
            }
            FillJson(success);
            return success;
        }
        public bool IsExists(SPRINT item, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<SPRINT>(dbName, Table_Sprint)
                                        .Find(x => x.Title.Equals(item.Title) && x.Index == item.Index);

                return result.Any();
            }
            catch (Exception)
            {
                return true;
            }
        }
        public SPRINT_DTO Update(SPRINT item, string dbName)
        {
            try
            {
                SPRINT_DTO inserted = null;
                var check = Db.Instance.GetCollection<SPRINT>(dbName, Table_Sprint)
                                        .Find(x => x.Title.Equals(item.Title));
                if (check.Any())
                    return inserted;

                Db.Instance.GetCollection<SPRINT>(dbName, Table_Sprint).ReplaceOne(x => x.Index == item.Index, item);
                inserted = item.Map<SPRINT, SPRINT_DTO>();
                FillJson(inserted);

                return inserted.Index > 0 ? inserted : null;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<SPRINT_DTO> UpdateMany(List<SPRINT> items, string dbName)
        {
            var fails = new List<SPRINT>();
            var success = new List<SPRINT_DTO>();
            SPRINT currentSPRINT = null;
            try
            {
                foreach (var item in items)
                {
                    currentSPRINT = item;
                    var inserted = Update(item, dbName);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }

            }
            catch (Exception)
            {
                fails.Add(currentSPRINT);
            }
            FillJson(success);
            return success;
        }
        public SPRINT_DTO Get(int id, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<SPRINT>(dbName, Table_Sprint)
                                        .Find(x => x.Index == id);
                SPRINT_DTO inserted = null;

                if (result.Any())
                {
                    inserted = result.FirstOrDefault().Map<SPRINT, SPRINT_DTO>();
                    FillJson(inserted);
                }

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<SPRINT_DTO> Gets(SPRINT item, string dbName)
        {
            try
            {
                var result = Db.Instance.GetCollection<SPRINT_DTO>(dbName, Table_Sprint).ToList();

                var filtered = result.Where(x => (item.Title is null || item.Title.Equals(string.Empty) || x.Title.Equals(item.Title))
                                                && (item.Progress == 0 || x.Progress == item.Progress)
                                                && (item.CreatedDate == null || x.CreatedDate == item.CreatedDate)
                                                && (item.UpdatedDate == null || x.UpdatedDate == item.UpdatedDate)
                                                && (item.Status == 0 || x.Status == item.Status));
                if (filtered != null && filtered.Any())
                    FillJson(filtered.ToList());

                return filtered.ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<SPRINT_DTO> DeleteMany(List<SPRINT> items, string dbName)
        {
            var fails = new List<SPRINT>();
            var success = new List<SPRINT_DTO>();
            var currentItem = items.FirstOrDefault();
            try
            {
                foreach (var item in items)
                {
                    currentItem = item;
                    var updated = Update(currentItem, dbName);
                    if (updated is null)
                        fails.Add(item);
                    else
                        success.Add(item.Map<SPRINT, SPRINT_DTO>());
                }
            }
            catch (Exception)
            {
                fails.Add(currentItem);
            }
            FillJson(success);
            return success;
        }
        private void FillJson(SPRINT_DTO inserted)
        {
            inserted.CreatedDateTimeStamp = inserted.CreatedDate.ToTimestamp();
            inserted.UpdatedDateTimeStamp = inserted.UpdatedDate.ToTimestamp();
            inserted.StatusType = GetStatus(inserted.Status);

        }
        private void FillJson(List<SPRINT_DTO> inserteds)
        {
            if (inserteds != null)
            {
                foreach (var inserted in inserteds)
                {
                    FillJson(inserted);
                }
            }
        }
        private StatusType GetStatus(int? status)
        {
            switch (status)
            {
                case 1:
                    return new StatusType("In Progress", "badge bg-success", true);
                case 2:
                    return new StatusType("Pending", "badge bg-warning", true);
                case 3:
                    return new StatusType("Complete", "badge bg-success", true);
                default:
                    return new StatusType("Cancle", "badge bg-danger");
            }
        }
    }
}