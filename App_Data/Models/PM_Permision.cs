﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;

/// <summary>
/// Summary description for PM_Project
/// </summary>
public partial class PMRes
{
    [DataContract(Name = "Permision")]
    public class PER
    {
        [DataMember] public int Id { get; set; }
        [DataMember] public string Name { get; set; }
        [DataMember] public DateTime CreatedDate { get; set; }
        [DataMember] public DateTime UpdatedDate { get; set; }
        [DataMember] public bool Status { get; set; }
    }
    [DataContract(Name = "PermisionDTO")]
    public class PER_DTO
    {
        [DataMember] public int Id { get; set; }
        [DataMember] public string Name { get; set; }
        [DataMember] public DateTime CreatedDate { get; set; }
        [DataMember] public DateTime UpdatedDate { get; set; }
        [DataMember] public double CreatedDateTimeStamp { get; set; }
        [DataMember] public double UpdatedDateTimeStamp { get; set; }
        [DataMember] public bool Status { get; set; }
        [DataMember] public StatusType StatusType { get; set; }
    }

    public sealed class PER_DAO
    {
        private static PER_DAO instance = null;
        public static PER_DAO Instance
        {
            get
            {
                instance = instance ?? new PER_DAO();
                return instance;
            }
        }
        private PER_DAO() { }
        public PER_DTO New(PER newItem)
        {
            try
            {
                PER_DTO inserted = null;

                if (IsExists(newItem.Name))
                    return inserted;

                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Permision_Insert)
                                           .AddParam("@Name", newItem.Name)
                                           .AddParam("@CreatedDate", DateTime.UtcNow)
                                           .Exec().ToList<PER_DTO>();

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public List<PER_DTO> NewMany(List<PER> newItem)
        {
            var fails = new List<PER>();
            var success = new List<PER_DTO>();
            PER currentPos = null;
            try
            {
                foreach (var item in newItem)
                {
                    currentPos = item;
                    var inserted = New(item);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }
            }
            catch (Exception)
            {
                fails.Add(currentPos);
            }

            return success;
        }
        public bool IsExists(string name)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Permision_IsExists)
                                           .AddParam("@Name", name)
                                           .Exec().ToList<PER_DTO>();
                return inserteds.Any();
            }
            catch (Exception)
            {
                return true;
            }
        }
        public PER_DTO Update(PER item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Permision_Update)
                                           .AddParam("@Name", item.Name)
                                           .AddParam("@UpdatedDate", DateTime.UtcNow)
                                           .AddParam("@Status", item.Status)
                                           .Exec().ToList<PER_DTO>();
                PER_DTO inserted = null;

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<PER_DTO> UpdateMany(List<PER> items)
        {
            var fails = new List<PER>();
            var success = new List<PER_DTO>();
            PER currentPos = null;
            try
            {
                foreach (var item in items)
                {
                    currentPos = item;
                    var inserted = Update(item);
                    if (inserted is null)
                        fails.Add(item);
                    else
                        success.Add(inserted);
                }

            }
            catch (Exception)
            {
                fails.Add(currentPos);
            }

            return success;
        }
        public PER_DTO Get(int id)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Permision_GetById)
                                           .AddParam("@Id", id)
                                           .Exec().ToList<PER_DTO>();
                PER_DTO inserted = null;

                FillJson(inserteds);

                if (inserteds.Any())
                    inserted = inserteds.FirstOrDefault();

                return inserted;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<PER_DTO> Gets(PER item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Permision_Gets)
                                           .AddParam("@Name", item.Name)
                                           .AddParam("@Status", item.Status)
                                           .Exec().ToList<PER_DTO>();
                FillJson(inserteds);

                return inserteds;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool ChangeStatus(ref PER item)
        {
            try
            {
                var inserteds = Db.Instance.Store(PMRes.Instance.Sp_Hr_ChangeStatus)
                                           .AddParam("@Id", item.Id)
                                           .AddParam("@UpdatedDate", DateTime.UtcNow)
                                           .AddParam("@Status", item.Status)
                                           .Exec().ToList<PER>();
                var check = inserteds.Any();
                if (check)
                    item = inserteds.FirstOrDefault();

                return check;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public List<PER_DTO> DeleteMany(List<PER> items)
        {
            var fails = new List<PER>();
            var success = new List<PER_DTO>();
            var currentPos = items.FirstOrDefault();
            try
            {
                foreach (var item in items)
                {
                    currentPos = item;
                    if (!ChangeStatus(ref currentPos))
                        fails.Add(currentPos);
                    else
                        success.Add(currentPos.Map<PER, PER_DTO>());
                }
            }
            catch (Exception)
            {
                fails.Add(currentPos);
            }

            FillJson(success);

            return success;
        }
        private void FillJson(List<PER_DTO> inserteds)
        {
            if (inserteds != null)
            {
                foreach (var inserted in inserteds)
                {
                    inserted.CreatedDateTimeStamp = inserted.CreatedDate.ToTimestamp();
                    inserted.UpdatedDateTimeStamp = inserted.UpdatedDate.ToTimestamp();
                    inserted.StatusType = GetStatus(inserted.Status);
                }
            }
        }
        public StatusType GetStatus(bool status)
        {
            switch (status)
            {
                case true:
                    return new StatusType("Active", "badge bg-success", true);
                case false:
                default:
                    return new StatusType("Inactive", "badge bg-danger");
            }
        }
    }
}