﻿/// <summary>
/// Summary description for PMRes
/// </summary>
public sealed partial class PMRes
{
    private static PMRes instance = null;
    private PMRes() { }
    public static PMRes Instance
    {
        get { instance = instance ?? new PMRes(); return instance; }
    }
}