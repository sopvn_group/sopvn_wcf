﻿using MongoDB.Driver;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for PM_Db
/// </summary>
public partial class PMRes
{
    public sealed class Db
    {
        private static Db instance = null;
        private string mongoConString = "mongodb://localhost:27017";
        private string sqlConString = ConfigurationManager.AppSettings.Get("ConnectionString");
        public static Db Instance
        {
            get
            {
                instance = instance ?? new Db();
                return instance;
            }
        }
        
        public SqlCommand Store(string store, short commandTimeout = 60, CommandType commandType = CommandType.StoredProcedure)
        {
            var db = new SqlConnection(sqlConString);
            
            var cmd = db.CreateCommand();

            cmd.CommandTimeout = commandTimeout;

            cmd.CommandText = store;
            cmd.CommandType = commandType;

            return cmd;
        }


        public IMongoCollection<T> GetCollection<T>(string dbName, string collectionName)
        {
            var client = new MongoClient(mongoConString);
            var db = client.GetDatabase(dbName);
            var collection = db.GetCollection<T>(collectionName);
            return collection;
        }

        public string CreateDb(string dbName)
        {
            var format = "Create Database {0} {1}.";

            try
            {
                var client = new MongoClient(mongoConString);
                var db = client.GetDatabase(dbName);
                db.CreateCollection(Table_Requirement);
                db.CreateCollection(Table_UserStory);
                db.CreateCollection(Table_Sprint);
                db.CreateCollection(Table_Task);
                db.CreateCollection(Table_SprintTest);
                db.CreateCollection(Table_PhysicTable);
                db.CreateCollection(Table_PhysicDb);
                
                return string.Format(format, dbName, "Successfull");
            }
            catch (System.Exception ex)
            {
                return string.Format(format, dbName, "Fail");
                throw ex;
            }
        }
    }
}
