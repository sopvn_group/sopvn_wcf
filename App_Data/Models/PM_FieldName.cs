﻿/// <summary>
/// Summary description for PM_FieldName
/// </summary>
public partial class PMRes
{
    public static string Table_Requirement = "Requirement";
    public static string Table_UserStory = "UserStory";
    public static string Table_Sprint = "Sprint";
    public static string Table_Task = "Task";
    public static string Table_SprintTest = "SprintTest";
    public static string Table_PhysicTable = "PhysicTable";
    public static string Table_PhysicDb = "PhysicDb";
}