USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Hr_Update]    Script Date: 7/23/2023 10:55:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[Sp_Pro_Hr_Update]
@Id int,
@ProjectId int,
@HrId int,
@PositionId int,
@IsCreator bit,
@UpdatedDate datetime
as
begin
	update ProjectHr
	set 
	ProjectId = ProjectId,HrId = @HrId,
	PositionId = PositionId,IsCreator = @IsCreator,
	UpdatedDate = @UpdatedDate
	where id = @id

	select * from ProjectHr where Id = @id
end