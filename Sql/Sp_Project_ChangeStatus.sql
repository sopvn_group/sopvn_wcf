USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Hr_ChangeStatus]    Script Date: 7/23/2023 12:30:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[Sp_Project_ChangeStatus]
@Id int,
@UpdatedDate nvarchar(100),
@Status bit
as
begin
	update Project
	set UpdatedDate = @UpdatedDate,
		[status] = @Status
	where Id = @Id

	select * from Project where Id = @Id and [Status] = @Status
end