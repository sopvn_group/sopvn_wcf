USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Hr_IsExists]    Script Date: 7/23/2023 10:31:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter proc [dbo].[Sp_Pro_Per_IsExists]
@ProjectId int,
@HrId int,
@PermisionId int
as
begin
	select * from GetProjectPermision()
	where ProjectId = @ProjectId 
	and HrId = @HrId
	and PermisionId = @PermisionId
	and [Status] = 1
end