USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[sp_Project_GetById]    Script Date: 7/23/2023 12:26:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[sp_Project_GetById] 
@id int
as
begin
	select * from Project where Id = @id
end
