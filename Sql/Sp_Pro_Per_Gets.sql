USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Hr_GetById]    Script Date: 7/23/2023 10:59:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_Pro_Per_Gets] 
@ProjectName nvarchar(500),
@HrName nvarchar(500),
@PermisionName nvarchar(500),
@Status bit
as
begin
	select * from GetProjectPermision()
	where 
		(dbo.IsNullOrEmpty(@ProjectName) = 1 or (ProjectName like @ProjectName or dbo.StringContain(ProjectName,@ProjectName) = 1))
	and	(dbo.IsNullOrEmpty(@HrName) = 1 or (HrName like @HrName or dbo.StringContain(HrName,@HrName) = 1))
	and	(dbo.IsNullOrEmpty(@PermisionName) = 1 or (PermisionName like @PermisionName or dbo.StringContain(PermisionName,@PermisionName) = 1))
	and	(@Status is null or [Status] = @Status)
end
