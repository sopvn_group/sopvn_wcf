USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Hr_Update]    Script Date: 7/22/2023 3:57:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[Sp_Hr_Update]
@id int,
@FirstName nvarchar(500),
@LastName nvarchar(500),
@FullName nvarchar(500),
@Email nvarchar(500),
@Password nvarchar(500),
@Contact nvarchar(500),
@UpdatedDate datetime,
@status bit 
as
begin
	update Hr
	set 
	FirstName = @FirstName,LastName = @LastName,FullName = @FullName,
	Email = @Email,[Password] = @Password,Contact = @Contact,
	UpdatedDate = @UpdatedDate,[status] = @status
	where id = @id

	select * from Hr where Id = @id and [Status] = @Status
end