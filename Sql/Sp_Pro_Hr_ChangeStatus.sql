USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Hr_ChangeStatus]    Script Date: 7/23/2023 11:28:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_Pro_Hr_ChangeStatus]
@Id int,
@UpdatedDate nvarchar(100),
@Status bit
as
begin
	update ProjectHr
	set UpdatedDate = @UpdatedDate,
		[status] = @Status
	where id = @id

	select * from ProjectHr where Id = @Id and [Status] = @Status
end