USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Position_Delete]    Script Date: 7/22/2023 1:32:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter proc [dbo].[Sp_Position_ChangeStatus]
@Id int,
@UpdatedDate nvarchar(100),
@Status bit
as
begin
	update Position
	set UpdatedDate = @UpdatedDate,
		[status] = @Status
	where id = @id

	select * from Position where Id = @Id and [Status] = @Status
end