USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Hr_IsExists]    Script Date: 7/23/2023 10:31:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_Pro_Hr_IsExists]
@ProjectId int,
@HrId int,
@PositionId int
as
begin
	select * from ProjectHr 
	where ProjectId = @ProjectId 
	and HrId = @HrId
	and PositionId = @PositionId
	and [Status] = 1
end