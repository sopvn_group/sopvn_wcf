USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Hr_IsExists]    Script Date: 7/22/2023 4:09:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_Project_IsExists]
@Name nvarchar(500)
as
begin
	select * from Project where [Name] like @Name and [Status] = 1
end