USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Hr_UpdateAvatar]    Script Date: 7/22/2023 3:58:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_Project_UpdateAvatar]
@id int,
@Avatar varchar(500),
@UpdatedDate datetime
as
begin
	update Project
	set 
	Avatar = @Avatar,
	UpdatedDate = @UpdatedDate
	where id = @id

	select * from Project where Id = @id
end