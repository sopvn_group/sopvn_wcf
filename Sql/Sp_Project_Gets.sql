USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Hr_Gets]    Script Date: 7/23/2023 12:27:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[Sp_Project_Gets]
@Name nvarchar(500) = null,
@Status bit = null
as
begin
	select * from Project
	where 
		(dbo.IsNullOrEmpty(@Name) = 1 or ([Name] like @Name or dbo.StringContain([Name],@Name) = 1))
	and (@Status is null or [Status] = @Status)
end