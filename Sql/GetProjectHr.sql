alter function GetProjectHr() returns table
as
	return
	(
		select ph.Id,ph.ProjectId,p.[Name] as ProjectName,ph.HrId,h.FullName as HrName,ph.PositionId,pos.[Name] as PositionName,
			ph.IsCreator,ph.CreatedDate,ph.UpdatedDate,ph.[Status]
		from ProjectHr ph,Project p,Hr h,Position pos
		where ph.ProjectId = p.Id and ph.HrId = h.Id and ph.PositionId = pos.Id
	)