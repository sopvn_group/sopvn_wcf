USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Hr_UpdatePassword]    Script Date: 7/22/2023 4:00:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[Sp_Hr_UpdatePassword]
@id int,
@Password varchar(500),
@UpdatedDate datetime
as
begin
	update Hr
	set 
	[Password] = @Password,
	UpdatedDate = @UpdatedDate
	where id = @id

	select * from Hr where Id = @id
end