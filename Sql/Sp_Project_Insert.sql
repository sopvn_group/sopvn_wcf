create proc [dbo].[Sp_Project_Insert]
@Name nvarchar(500),
@Document nvarchar(500),
@Scrum nvarchar(500),
@Avatar nvarchar(500),
@Connection nvarchar(500),
@Database nvarchar(500),
@CreatedDate datetime
as
begin
	Declare @Tmp TABLE(
		Id int
	)
    Insert into Project([Name],Document,Scrum,Avatar,Connection,[Database],CreatedDate,UpdatedDate,[Status])
	OUTPUT  inserted.Id INTO @Tmp
	values (@Name,@Document,@Scrum,@Avatar,@Connection,@Database,@CreatedDate,@CreatedDate,1)

	declare @Id int = (select top 1 Id from @Tmp)
	select top 1 * from Project where Id = @Id
end