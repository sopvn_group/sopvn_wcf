create proc Sp_Log_Insert
@Content nvarchar(max),
@DataJson ntext,
@IsError bit,
@CreatedDate datetime
as
begin
	insert into Logger(Content,DataJson,IsError,CreatedDate)
	values(@Content,@DataJson,@IsError,@CreatedDate)
end
