USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[sp_Position_Update]    Script Date: 7/20/2023 6:08:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[sp_Position_Update]
@id int,
@name nvarchar(100),
@status bit 
as
begin
	update position
	set 
	[name] = @name,
	[status] = @status
	where id = @id

	select * from Position where Id = @id and [Status] = @Status
end