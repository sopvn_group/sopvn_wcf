USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Hr_GetById]    Script Date: 7/23/2023 10:59:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_Pro_Hr_Gets] 
@ProjectName nvarchar(500),
@HrName nvarchar(500),
@PositionName nvarchar(500),
@Status bit
as
begin
	select * from GetProjectHr()
	where 
		(dbo.IsNullOrEmpty(@ProjectName) = 1 or (ProjectName like @ProjectName or dbo.StringContain(ProjectName,@ProjectName) = 1))
	and	(dbo.IsNullOrEmpty(@HrName) = 1 or (HrName like @HrName or dbo.StringContain(HrName,@HrName) = 1))
	and	(dbo.IsNullOrEmpty(@PositionName) = 1 or (PositionName like @PositionName or dbo.StringContain(PositionName,@PositionName) = 1))
	and	(@Status is null or [Status] = @Status)
end
