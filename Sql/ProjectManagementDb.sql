create database ProjectManagement

go
create table Logger(
	 Id int identity(1,1) constraint pk_logger_id primary key,
	 Content varchar(max) constraint default_logger_content default '',
	 DataJson ntext constraint default_logger_data default '',
	 IsError bit constraint default_logger_iserror default 0,
	 CreatedDate datetime constraint default_logger_createdDate default getDate()
)
go
create table Position(
	Id int identity(1,1) constraint pk_position_id primary key,
	[Name] nvarchar(200) constraint default_position_name default '',
	CreatedDate datetime constraint default_position_createdDate default getDate(),
	UpdatedDate datetime constraint default_position_UpdatedDate default getDate(),
	[Status] bit constraint default_position_status default 1
)
go
create table Permision(
	Id int identity(1,1) constraint pk_permision primary key,
	[Name] nvarchar(200) constraint default_permision_name default '',
	CreatedDate datetime constraint default_permision_createdDate default getDate(),
	UpdatedDate datetime constraint default_permision_UpdatedDate default getDate(),
	[Status] bit constraint default_permision_status default 1
)
go
create table Hr(
	Id int identity(1,1) constraint pk_hr_id primary key,
	FirstName nvarchar(50) constraint default_hr_firstName default '',
	LastName nvarchar(50) constraint default_hr_lastName default '',
	FullName nvarchar(100) constraint default_hr_fullName default '',
	Email varchar(200) constraint default_hr_email default '',
	[Password] varchar(500) constraint default_hr_password default '',
	Contact varchar(500) constraint default_hr_contact default '',
	[Address] nvarchar(1000) constraint default_hr_address default '',
	Avatar ntext constraint default_hr_avatar default '',
	CreatedDate datetime constraint default_hr_createdDate default getDate(),
	UpdatedDate datetime constraint default_hr_UpdatedDate default getDate(),
	HistoryUpdate ntext constraint default_hr_historyUpdate default '',
	HistoryLogin ntext constraint default_hr_historyLogin default '',
	IsLogged bit constraint default_hr_isLogged default 0,
	[Status] bit constraint default_hr_status default 1
)
go
create table Project(
	Id int identity(1,1) constraint pk_project_id primary key,
	[Name] nvarchar(500) constraint default_project_name default '',
	Document varchar(1000) constraint default_project_document default '',
	Scrum nvarchar(1000) constraint default_project_scrum default '',
	TotalTime int constraint default_project_totalTime default 0,
	Cost decimal constraint default_project_cost default 0,
	Avatar ntext constraint default_project_avatar default '',
	ImageInfo ntext constraint default_project_imageInfo default '',
	Connection ntext constraint default_project_connection default '',
	[Database] ntext constraint default_project_database default '',
	History ntext constraint default_hr_history default '',
	CreatedDate datetime constraint default_project_createdDate default getDate(),
	UpdatedDate datetime constraint default_project_UpdatedDate default getDate(),
	[Status] int constraint default_project_status default 1
)
go
create table ProjectHr(
	Id int identity(1,1),
	ProjectId int constraint fk_ph_project_id references Project(Id),
	HrId int constraint fk_ph_hr_id references Hr(Id),
	PositionId int constraint fk_ph_position_id references Position(Id),
	constraint pk_projectHr primary key(ProjectId,HrId,PositionId),
	IsCreator bit constraint default_ph_isCreator default 0,
	CreatedDate datetime constraint default_ph_createdDate default getDate(),
	UpdatedDate datetime constraint default_ph_UpdatedDate default getDate(),
	[Status] bit constraint default_ph_status default 1
)
go
create table ProjectPermision(
	Id int identity(1,1),
	ProjectId int constraint fk_pp_project_id references Project(Id),
	HrId int constraint fk_pp_hr_id references Hr(Id),
	constraint pk_ProjectPermision primary key(ProjectId,HrId),
	PermisionId int constraint fk_pp_permision_id references Permision(Id),
	CreatedDate datetime constraint default_pp_createdDate default getDate(),
	UpdatedDate datetime constraint default_pp_UpdatedDate default getDate(),
	[Status] bit constraint default_pp_status default 1
)

create function IsNullOrEmpty(@text nvarchar(max)) returns bit
as
begin
	declare @check bit = 0;
	if(@text is null or @text like '')
		begin
			set @check = 1;
		end
	return @check
end

create function StringContain(@baseText nvarchar(max),@text nvarchar(max)) returns bit
as
begin
	declare @check bit = 0;
	if(@baseText like '%' + @text + '%')
		begin
			set @check = 1;
		end
	return @check
end