create function GetProjectPermision() returns table
as 
	return (
		select pp.Id,pp.ProjectId,p.[Name] as ProjectName,pp.HrId,h.FullName as HrName,
		pp.PermisionId,per.[Name] as PermisionName,pp.CreatedDate,pp.UpdatedDate,pp.[Status]
		from ProjectPermision pp,Project p,Hr h,Permision per
		where pp.ProjectId = p.Id and pp.HrId = h.Id and pp.PermisionId = per.Id
	)