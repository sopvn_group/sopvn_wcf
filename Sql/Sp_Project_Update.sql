USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[sp_Project_Update]    Script Date: 7/22/2023 4:13:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter proc [dbo].[Sp_Project_Update]
@Id int,
@Name nvarchar(500),
@Document nvarchar(500),
@Scrum nvarchar(500),
@Connection nvarchar(500),
@Database nvarchar(500),
@CreatedDate datetime
as
begin
	update project
	set
	[Name] = @Name,
	Document = @Document,
	Scrum = @Scrum,
	Connection = @Connection,
	[Database] = @Database,
	CreatedDate = @CreatedDate
	where Id = @Id

	select * from Project where Id = @Id
end 