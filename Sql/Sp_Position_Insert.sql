USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[sp_Position_Create]    Script Date: 7/13/2023 7:16:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_Position_Insert]
@Name nvarchar(500),
@CreatedDate datetime
as
begin
	Declare @Tmp TABLE(
		Id int
	)
    Insert into Position([Name],CreatedDate,UpdatedDate,[Status])
	OUTPUT  inserted.Id INTO @Tmp
	values (@Name,@CreatedDate,@CreatedDate,1)

	declare @Id int = (select top 1 Id from @Tmp)
	select top 1 * from Position where Id = @Id
end