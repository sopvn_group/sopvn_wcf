USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Hr_Insert]    Script Date: 7/23/2023 10:29:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter proc [dbo].[Sp_Pro_Per_Insert]
@ProjectId int,
@HrId int,
@PermisionId int,
@CreatedDate datetime
as
begin
	Declare @Tmp TABLE(
		Id int
	)
    Insert into ProjectPermision(ProjectId,HrId,PermisionId,CreatedDate,UpdatedDate,[Status])
	OUTPUT  inserted.Id INTO @Tmp
	values (@ProjectId,@HrId,@PermisionId,@CreatedDate,@CreatedDate,1)

	declare @Id int = (select top 1 Id from @Tmp)
	select top 1 * from GetProjectPermision() where Id = @Id
end