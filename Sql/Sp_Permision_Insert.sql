create proc [dbo].[Sp_Permision_Insert]
@Name nvarchar(500),
@CreatedDate datetime
as
begin
	Declare @Tmp TABLE(
		Id int
	)
    Insert into Permision([Name],CreatedDate,UpdatedDate,[Status])
	OUTPUT  inserted.Id INTO @Tmp
	values (@Name,@CreatedDate,@CreatedDate,1)

	declare @Id int = (select top 1 Id from @Tmp)
	select top 1 * from Permision where Id = @Id
end