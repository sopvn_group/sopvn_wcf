USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Hr_GetById]    Script Date: 7/23/2023 10:59:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_Pro_Hr_GetById] 
@id int
as
begin
	select * from GetProjectHr()
	where Id = @id
end
