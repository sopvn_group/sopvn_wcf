USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[sp_Position_GetById]    Script Date: 7/18/2023 11:33:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_Position_GetById] 
@id int
as
begin
	select * from Position where Id = @id
end
