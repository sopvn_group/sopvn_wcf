USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Position_Gets]    Script Date: 7/18/2023 11:33:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[Sp_Position_Gets]
@Name nvarchar(500) = null,
@Status bit = null
as
begin
	select * from Position
	where 
		(dbo.IsNullOrEmpty(@Name) = 1 or ([Name] like @Name or dbo.StringContain([Name],@Name) = 1))
	and	(@Status is null or [Status] = @Status)
end