USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Position_IsExists]    Script Date: 7/18/2023 11:35:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[Sp_Position_IsExists]
@Name nvarchar(500)
as
begin
	select * from Position where [Name] like @Name and [Status] = 1
end