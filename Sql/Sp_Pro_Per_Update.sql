USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Hr_Update]    Script Date: 7/23/2023 10:55:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter proc [dbo].[Sp_Pro_Per_Update]
@Id int,
@ProjectId int,
@HrId int,
@PermisionId int,
@UpdatedDate datetime
as
begin
	update ProjectPermision
	set 
	ProjectId = @ProjectId,HrId = @HrId,
	PermisionId = @PermisionId,
	UpdatedDate = @UpdatedDate
	where id = @id

	select * from GetProjectPermision() where Id = @id
end